<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    if (\Illuminate\Support\Facades\Auth::check()){
//        return redirect('dashboard');
//    }
    return view('index');
})->name('public.index');

Route::get('/custom-searches', 'PagesController@advancedSearch')->name('public.custom.search');
Route::get('/searches', 'PagesController@search')->name('public.basic.search');

Auth::routes();

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('dashboard');

    //Routes para usuários
    Route::get('/user/index', 'UserController@index')->name('dashboard.user.index');
    Route::get('/user/create', 'UserController@create')->name('dashboard.user.create');
    Route::post('/user/store', 'UserController@store')->name('dashboard.user.store');
//    Route::resource('user', 'UserController', ['only' => [ 'index', 'create', 'store', 'edit', 'update', 'destroy']]);

    //Routes para o objeto de aprend
    Route::get('/object/index', 'LobjectController@index')->name('dashboard.object.index');
    Route::get('/object/create', 'LobjectController@create')->name('dashboard.object.create');
    Route::post('/object/store', 'LobjectController@store')->name('dashboard.object.store');
    Route::resource('object', 'LobjectController',['only' => ['update']]);

    //IF is admin - INDEX
    Route::get('/object/index_admin', 'LobjectController@index')->name('dashboard.object.index_admin');

    //List Objects
    Route::get('object/listObjects/{state_id}', 'LobjectController@listObjects')->name('list.objects')
        ->where('state_id', '[0-9]+');

    Route::get('/back/{state_id}', 'LobjectController@btnBack')
        ->name('btn.back')
        ->where('state_id', '[0-9]+');
    //

    Route::get('object/analyze/{object_id}', 'LobjectController@analyze_object')
        ->name('opt.analyze')
        ->where('object_id', '[0-9]+');

    Route::get('object/to_approve/{object_id}', 'LobjectController@to_approve')
        ->name('opt.to_approve')
        ->where('object_id', '[0-9]+');

    Route::get('object/turn_to_analysis/{object_id}', 'LobjectController@turn_to_analysis')
        ->name('opt.turn_to_analysis')
        ->where('object_id', '[0-9]+');

    Route::post('object/rej_def', 'LobjectController@reject_definitively')->name('opt.rej_def');
    Route::post('object/rej_adj', 'LobjectController@reject_adjustment')->name('opt.rej_adj');

    Route::get('object/delete/{object_id}', 'LobjectController@delete_logically')
        ->name('opt.delete_logically')
        ->where('object_id', '[0-9]+');

    Route::get('object/deleteEver/{object_id}', 'LobjectController@delete_ever')
        ->name('opt.delete_ever')
        ->where('object_id', '[0-9]+');


    Route::get('object/edit/{object_id}', 'LobjectController@edit_object')
        ->name('opt.edit')
        ->where('object_id', '[0-9]+');


    Route::get('/object/viewPendency/{object_id}', 'LobjectController@view_pendency')
        ->name('opt.pend')
        ->where('object_id', '[0-9]+');


});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('accessibility', 'AccessibilityController', ['only' => [ 'index']]);

Route::resource('map', 'MapController', ['only' => [ 'index']]);


//Options for object
Route::get('object/view/{object_id}', 'LobjectController@view_object')
    ->name('opt.view')
    ->where('object_id', '[0-9]+');