<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLearnObjectStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lobject_state', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lobject_id');
            $table->integer('state_id');
            $table->dateTime('order');
            $table->foreign('lobject_id')->references('id')->on('lobjects')->onDelete('cascade');
            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lobject_state');
    }
}
