<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisabilityObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disability_lobject', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('disability_id');
            $table->integer('lobject_id');
            $table->foreign('disability_id')->references('id')->on('disabilities')->onDelete('cascade');
            $table->foreign('lobject_id')->references('id')->on('lobjects')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disability_lobject');
    }
}
