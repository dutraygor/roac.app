<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLearnObjectStateIdTableLearnObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lobjects', function (Blueprint $table) {
            $table->integer('los_id')->nullable();
            $table->foreign('los_id')->references('id')->on('lobject_state')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lobjects', function (Blueprint $table) {
            $table->dropColumn('los_id');
        });
    }
}
