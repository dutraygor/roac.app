<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLearnObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lobjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('author');
            $table->string('description');
            $table->string('goal');
            $table->string('notes');
            $table->string('theme');
            $table->integer('modality_id');
            $table->string('language');
            $table->string('country');
            $table->string('key_words');
            $table->string('format');
            $table->integer('resource_type_id');
            $table->float('size');
            $table->string('path');
//            $table->string('key_words');
            $table->string('rights');
            $table->string('license');
            $table->string('reason_of_rejection')->nullable();
            $table->integer('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lobjects');
    }
}
