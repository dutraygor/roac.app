<?php

use App\Models\ResourceType;
use Illuminate\Database\Seeder;

class resourceTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createResources();
    }

    public function createResources(){
        ResourceType::create([
            'name' => 'Documento',
        ]);

        ResourceType::create([
            'name' => 'Presentación',
        ]);

        ResourceType::create([
            'name' => 'Imagen',
        ]);

        ResourceType::create([
            'name' => 'Video',
        ]);

        ResourceType::create([
            'name' => 'Audio',
        ]);

        ResourceType::create([
            'name' => 'URL',
        ]);

        ResourceType::create([
            'name' => 'Software Educativo',
        ]);
    }
}
