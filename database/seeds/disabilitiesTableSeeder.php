<?php

use App\Models\Disability;
use Illuminate\Database\Seeder;

class disabilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createDisabilities();
    }

    public function createDisabilities(){
        Disability::create([
            'name' => 'Accesible para personas ciegas',
        ]);

        Disability::create([
            'name' => 'Accesible para personas sordas',
        ]);

        Disability::create([
            'name' => 'Otra discapacidad',
        ]);

    }
}
