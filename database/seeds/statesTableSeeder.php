<?php

use Illuminate\Database\Seeder;
use App\Models\State;

class statesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->createStates();

    }

    public function createStates()
    {

        State::create([
            'id' => 0,
            'description' => 'Borrador'
        ]);

        State::create([
            'id' => 1,
            'description' => 'Esperando para revisión'
        ]);

        State::create([
            'id' => 2,
            'description' => 'Aprobado'
        ]);

        State::create([
            'id' => 3,
            'description' => 'Rechazado por ajustes'
        ]);

        State::create([
            'id' => 4,
            'description' => 'Rechazado Definitivamente'
        ]);
        State::create([
            'id' => 5,
            'description' => 'Eliminado'
        ]);
    }


}
