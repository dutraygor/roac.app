<?php

use App\Models\Subject;
use Illuminate\Database\Seeder;

class subjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createSubjects();
    }

    public function createSubjects()
    {
        Subject::create([
            'name' => 'Agronomía'
        ]);

        Subject::create([
            'name' => 'Medicina Veterinaria '
        ]);

        Subject::create([
            'name' => 'Zootécnia'
        ]);

        Subject::create([
            'name' => 'Artes Plásticas, Visuales y afines '
        ]);

        Subject::create([
            'name' => 'Diseño'
        ]);

        Subject::create([
            'name' => 'Música'
        ]);

        Subject::create([
            'name' => 'Otros programas asociados a Bellas Artes'
        ]);

        Subject::create([
            'name' => 'Publicidad y afines'
        ]);

        Subject::create([
            'name' => 'Bacteriología'
        ]);

        Subject::create([
            'name' => 'Enfermería'
        ]);

        Subject::create([
            'name' => 'Instrumentación quirúrgica'
        ]);

        Subject::create([
            'name' => 'Medicina'
        ]);

        Subject::create([
            'name' => 'Odontología'
        ]);

        Subject::create([
            'name' => 'Optometría, otros programas de Ciencias de la Salud'
        ]);

        Subject::create([
            'name' => 'Salud Pública'
        ]);

        Subject::create([
            'name' => 'Terapias'
        ]);

        Subject::create([
            'name' => 'Antropología, Artes Liberales'
        ]);

        Subject::create([
            'name' => 'Bibliotecología, otros de Ciencias Sociales y Humanas'
        ]);

        Subject::create([
            'name' => 'Comunicación Social, Periodismo y afines '
        ]);

        Subject::create([
            'name' => 'Derecho y afines '
        ]);

        Subject::create([
            'name' => 'Filosofía, Teología y afines '
        ]);

        Subject::create([
            'name' => 'Geografía e Historia '
        ]);

        Subject::create([
            'name' => 'Lenguas Modernas, Literatura, Lingüística y afines'
        ]);

        Subject::create([
            'name' => 'Sociología, Trabajo Social y afines'
        ]);

        Subject::create([
            'name' => 'Administración'
        ]);

        Subject::create([
            'name' => 'Contaduría Pública'
        ]);

        Subject::create([
            'name' => 'Economía'
        ]);

        Subject::create([
            'name' => 'Arquitectura y afines'
        ]);

        Subject::create([
            'name' => 'Ingeniería Administrativa y afines'
        ]);

        Subject::create([
            'name' => 'Ingeniería Agrícola, Forestal y afines'
        ]);

        Subject::create([
            'name' => 'Ingeniería Agroindustrial, Alimentos y afines'
        ]);

        Subject::create([
            'name' => 'Ingeniería Agronómica, Pecuaria y afines'
        ]);

        Subject::create([
            'name' => 'Ingeniería Ambiental, Sanitaria y afines'
        ]);

        Subject::create([
            'name' => 'Ingeniería De Sistemas, Telemática y afines'
        ]);

        Subject::create([
            'name' => 'Ingeniería Electrónica, Telecomunicaciones y afines'
        ]);

        Subject::create([
            'name' => 'Ingeniería Industrial y afines'
        ]);

        Subject::create([
            'name' => 'Biología Microbiología y afines'
        ]);

        Subject::create([
            'name' => 'Botánica'
        ]);

        Subject::create([
            'name' => 'Zoología'
        ]);

        Subject::create([
            'name' => 'Física'
        ]);

        Subject::create([
            'name' => 'Geología, otros programas de Ciencias Naturales'
        ]);

        Subject::create([
            'name' => 'Matemáticas, Estadística y afines'
        ]);

        Subject::create([
            'name' => 'Química y afines'
        ]);


    }
}
