<?php

use App\Models\Content;
use Illuminate\Database\Seeder;

class contentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createContents();
    }

    public function createContents()
    {
        Content::create([
            'name' => 'Archivo único',
            'subject_id' => '1'
        ]);

        Content::create([
            'name' => 'Carpeta comprimida',
            'subject_id' => '2'
        ]);

        Content::create([
            'name' => 'Vínculo a una URL',
            'subject_id' => '3'
        ]);


    }
}
