<?php

use App\Models\Course;
use Illuminate\Database\Seeder;

class coursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createCourses();
    }

    public function createCourses(){
        Course::create([
            'name' => 'Agronomía, Veterinaria y afines',
        ]);

        Course::create([
            'name' => 'Bellas Artes',
        ]);

        Course::create([
            'name' => 'Ciencias de la Educación',
        ]);

        Course::create([
            'name' => 'Ciencias de la Salud',
        ]);

        Course::create([
            'name' => 'Ciencias Sociales y Humanas',
        ]);

        Course::create([
            'name' => 'Economía, Administración, Contaduría y afines',
        ]);

        Course::create([
            'name' => 'Ingeniería, Arquitectura, Urbanismo y afines',
        ]);

        Course::create([
            'name' => 'Matemáticas y Ciencias Naturales',
        ]);
    }
}