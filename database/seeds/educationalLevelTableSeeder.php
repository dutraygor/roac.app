<?php

use App\Models\EducationalLevel;
use Illuminate\Database\Seeder;

class educationalLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createEducLevel();
    }

    public function createEducLevel(){
        EducationalLevel::create([
            'level' => 'Educación Primaria',
        ]);

        EducationalLevel::create([
            'level' => 'Educación Secundaria',
        ]);

        EducationalLevel::create([
            'level' => 'Educación Superior',
        ]);

        EducationalLevel::create([
            'level' => 'Maestría',
        ]);

        EducationalLevel::create([
            'level' => 'Doctorado',
        ]);
    }
}
