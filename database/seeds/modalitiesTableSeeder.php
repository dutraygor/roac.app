<?php

use App\Models\Modality;
use Illuminate\Database\Seeder;

class modalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->crateModalities();
    }
    public function crateModalities(){
        Modality::create([
            'name' => 'Objeto Informativo',
        ]);

        Modality::create([
            'name' => 'Objeto de Aprendizaje',
        ]);

        Modality::create([
            'name' => 'Recurso Educativo',
        ]);
    }
}
