
<?php

use Illuminate\Database\Seeder;
use App\Models\State;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(usersTableSeeder::class);
        $this->call(statesTableSeeder::class);
        $this->call(modalitiesTableSeeder::class);
        $this->call(educationalLevelTableSeeder::class);
        $this->call(coursesTableSeeder::class);
        $this->call(subjectsTableSeeder::class);
        $this->call(contentsTableSeeder::class);
        $this->call(disabilitiesTableSeeder::class);
        $this->call(resourceTypesTableSeeder::class);
    }





}
