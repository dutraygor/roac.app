<footer class="main-footer" id="footer">

    <div class="container-fluid">
        <div class="row conteudo-rodape" style="vertical-align: middle">

            <div class="col-md-2" align="center">
                <a title="ACACIA" href="https://acacia.digital/pt/" target="_blank">
                    <img style="height: 2.1em; " src="{{ url('img/logo_acacia_small.png') }}" alt="ACACIA"/>
                </a>
            </div>

            <div class="col-md-3" align="center">
                <strong>@lang('footer.full_name') </strong>
                <a style="text-decoration: none" href="{{route('dashboard')}}">
                    {{config('app.name')}}
                </a>
            </div>

            <div class="col-md-2" align="center">
                <a style="text-decoration: none"  href="{{route('accessibility.index')}}">
                    <i class="fa fa-fw  fa-wheelchair"></i>
                    @lang('footer.accessibility')
                </a>
            </div>
            <div class="col-md-2" align="center">
                <a style="text-decoration: none"  href="{{route('map.index')}}">
                    <i class="fa fa-fw  fa-map-o"></i>
                    @lang('footer.map')
                </a>
            </div>
            <div class="col-md-2" align="center">
                <strong> @lang('footer.version') </strong>{{config('app.version')}}
            </div>
            <div class="col-md-1">
            </div>

        </div>
    </div>
</footer>

<style>

    .conteudo-rodape {
        font-size: 14px;
    }

    .conteudo-rodape a, p {
        font-family: "PT Sans", "Helvetica Neue", Arial, sans-serif;
        font-weight: normal;
        text-decoration: none;
    }

    .conteudo-rodape a{
        color: #0075a8;
    }

    .conteudo-rodape p{
        color: #101010;
    }

    .conteudo-rodape a:hover {
        color: #00405c;
        text-decoration: none;
    }


    .conteudo-rodape h1, h2, h3, h4, h5, h6 {
        font-weight: bold;
    }


</style>