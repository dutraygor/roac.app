@extends('adminlte::page')
{{--trocar de quem extend--}}
@section('content_header')


    <h1>@lang('adminlte::adminlte.search')</h1>

@endsection

@section('content')
    {{--@push('menu-items')--}}
    {{--asasas--}}
    {{--@endpush--}}
    <div class="container box box-solid">
        <div class='row'>
            <div class="col-md-10">
                {{--<div class='row col-md-8 col-md-offset-2'>--}}
                {{--<div class="panel panel-default">--}}

                <form method="GET" action="{{ route('public.custom.search') }}">
                    {{--<div class="input-group input-group-md col-10 offset-4 ">--}}
                    <div class="input-group row col-md-8 col-md-offset-2">
                        <input  type="text" class="form-control" placeholder="Search" name="search">
                        {{old('search')}}
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-info btn-flat">Go!</button>
                        </span>

                    </div>

                    <div class="input-group row col-md-8 col-md-offset-2">
                    <div style="margin-left: 1rem" class="checkbox" align="center">
                        <label>
                            <input type="checkbox" onclick="mostrar()" id="advanced">
                            @lang('adminlte::adminlte.advanced_search')
                        </label>
                    </div>
                    </div>



                    <div id="1" class="row col-md-12 input-group" style="display: none">
                        <div class="col-md-6 col-md-offset-0">
                            <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="with_author">
                        </span>
                                <input type="text" class="form-control" name='author' placeholder="@lang('adminlte::adminlte.author')"
                                       value="{{ old('author') }}"/>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="with_theme">
                        </span>
                                <input type="text" class="form-control" name='theme' placeholder="@lang('adminlte::adminlte.theme')"
                                       value="{{ old('theme') }}"/>
                            </div>
                        </div>
                    </div>

                    <div id="2" class="row col-md-12 input-group" style="display: none">
                        <div class="col-md-6 col-md-offset-0">
                            <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="with_educ_level">
                        </span>
                                <select class="form-control" name='educ_level'>
                                    <option value="" disabled selected hidden>@lang('adminlte::adminlte.educ_level')</option>
                                    @foreach(\App\Models\EducationalLevel::all() as $educlvl)
                                        <option value="{{$educlvl->level}}">{{$educlvl->level}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="with_content">
                        </span>
                                <input type="text" class="form-control" name='content' placeholder="@lang('adminlte::adminlte.content')"
                                       value="{{ old('content') }}"/>
                            </div>
                        </div>
                    </div>


                    <div id="3" class="row col-md-12 input-group" style="display: none">
                        <div class="col-md-6 col-md-offset-0">
                            <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="with_disability">
                        </span>
                                <select class="form-control" name='disability'>
                                    <option value="0" disabled selected hidden>@lang('adminlte::adminlte.disability')</option>
                                    @foreach(\App\Models\Disability::all() as $disability)
                                        <option value="{{$disability->name}}">{{$disability->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="with_res_type">
                        </span>

                                <select class="form-control" name='res_type'>
                                    <option value="0" disabled selected hidden>@lang('adminlte::adminlte.resource_type')</option>
                                    @foreach(\App\Models\ResourceType::all() as $res_type)
                                        <option value="{{$res_type->name}}">{{$res_type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                </form>

                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>

@if(Auth::check())
        <script type="text/javascript">
            window.onload = function(){
                document.getElementById('advanced').checked = true;
                mostrar();
            }
        </script>
@endif
        <script type="text/javascript">
            function mostrar() {
                if (document.getElementById('advanced').checked == false) {
                    document.getElementById('1').style.display = 'none';
                    document.getElementById('2').style.display = 'none';
                    document.getElementById('3').style.display = 'none';
                }else {
                    document.getElementById('1').style.display = 'block';
                    document.getElementById('2').style.display = 'block';
                    document.getElementById('3').style.display = 'block';
                }

                //document.getElementsByClassName('col-md-12').style.display = 'block';
            }
        </script>


        <div class="box">

            <div class="box-header with-border">

                <div class="box-body">
                    <div class="table-responsive">

                        <table class="table">

                            <thead>
                            <tr>
                                <th style="max-width: 20%">@lang('adminlte::adminlte.title')</th>
                                <th style="max-width: 20%">@lang('adminlte::adminlte.author')</th>
                                <th style="max-width: 20%">@lang('adminlte::adminlte.description')</th>
                                <th style="max-width: 20%">@lang('adminlte::adminlte.format')</th>
                                <th style="max-width: 20%">@lang('adminlte::adminlte.actions')</th>

                            </tr>
                            </thead>

                            <tbody>
                            @foreach($results as $result)
                                <tr>
                                    <td>{{$result->title}}</td>
                                    <td>{{$result->author}}</td>
                                    <td>{{$result->description}}</td>
                                    <td>{{$result->format}}</td>
                                    <td>
                                        <a href="{{ route('opt.view', ['object_id' => $result->los->lobject_id]) }}"
                                           style="margin-right: 1rem"
                                           title="@lang('adminlte::adminlte.view')" class="btn btn-default"><i
                                                    class="fa fa-fw fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    {{ $results->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection