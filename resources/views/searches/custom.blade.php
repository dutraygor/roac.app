@extends('adminlte::page')
{{--trocar de quem extend--}}
@section('content_header')


    <h1>Filter</h1>

@endsection

@section('content')
    {{--@push('menu-items')--}}
    {{--asasas--}}
    {{--@endpush--}}
    <div class='row'>
        <div class="col-md-10">
            {{--<div class='row col-md-8 col-md-offset-2'>--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="input-group input-group-md col-10 offset-4">--}}
                        {{--<input type="text" class="form-control" placeholder="Search">--}}
                        {{--<span class="input-group-btn">--}}
                                {{--<a href="{{route('public.index')}}" type="button" class="btn btn-info btn-flat">Go!</a>--}}
                            {{--</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <form method='post' action="#" class='form-horizontal'>
                {!! csrf_field() !!}
                {{--<div class="col-lg-5">--}}
                {{--<label> Filter by: </label>--}}
                {{--</div>--}}



                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="withauthor">
                        </span>
                        <input type="text" class="form-control" id="busca" name='busca' placeholder="Author"
                               value="{{ old('busca') }}"/>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="withauthor">
                        </span>
                        <input type="text" class="form-control" id="busca" name='busca' placeholder="Theme"
                               value="{{ old('busca') }}"/>
                    </div>
                </div>


                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="withauthor">
                        </span>
                        <select class="form-control" name='busca'>
                            <option>Educational Level</option>
                            <option class="disabled display-1">Elementary</option>
                            <option>High</option>
                            <option>Bachelor</option>

                        </select>
                    </div>
                </div>


                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="withauthor">
                        </span>
                        <input type="text" class="form-control" id="busca" name='busca' placeholder="Content"
                               value="{{ old('busca') }}"/>
                    </div>
                </div>


                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="withauthor">
                        </span>
                        <input type="text" class="form-control" id="busca" name='busca' placeholder="Disability"
                               value="{{ old('busca') }}"/>
                    </div>
                </div>


                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="withauthor">
                        </span>
                        <input type="text" class="form-control" id="busca" name='busca' placeholder="Resource Type"
                               value="{{ old('busca') }}"/>
                    </div>
                </div>

                <div class="col-lg-12">
                    <br><a class="btn btn-primary btn-block" href="#" name="filter" type="submit"> Filter </a>
                </div>
            </form>
        </div>
    </div>

@endsection