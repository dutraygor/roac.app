{{--@extends('layouts.app')--}}
@extends('adminlte::page')

@section('content_header')
    <h1 align="center">@lang('footer.map')</h1>
@endsection

@section('content')

    <a style="align-items: center;  display: flex;  flex-direction: row;  flex-wrap: wrap;  justify-content: center;">
        <img src="{{ url('img/logo_small.png') }}">
    </a>

    <div class="page-content" id="map" style="margin-left: 10%; margin-right: 10%">
        <div class="row">

            <!--Right Side-->
            <div class="col-md-6">
                <ul>
                    <li class="menu">
                        <a href="{{route('dashboard')}}">
                            <i class="fa fa-fw  fa-folder-o"></i>
                            {{config('app.name')}} - Dashboard
                        </a>
                    </li>

                    <li class="menu">
                        <a href="#">
                            <i class="fa fa-fw  fa-folder-o"></i>
                            @lang('adminlte::adminlte.published_objects')
                        </a>
                    </li>

                    @if(Auth::check())
                        @if(Auth::user()->isAdmin())
                            <li class="menu">
                                <a href="{{ route('dashboard.object.index_admin') }}" target="_blank">
                                    <i class="fa fa-fw  fa-folder-open-o"></i>
                                    @lang('adminlte::adminlte.all_objects')
                                </a>

                        @elseif(Auth::user()->isCollaborator())
                            <li class="menu">
                                <a href="{{ route('dashboard.object.index') }}" target="_blank">
                                    <i class="fa fa-fw  fa-folder-open-o"></i>
                                    @lang('adminlte::adminlte.my_objects')
                                </a>
                                @endif

                                <ul style="margin-top: -2px">
                                    <li class="sub_menu">
                                        <a href="{{route('dashboard.object.create')}}" target="_blank">
                                            <i class="fa fa-fw  fa-level-up"
                                               style="-webkit-transform: rotate(95grad);-moz-transform: rotate(95grad);-ms-transform: rotate(95grad);-o-transform: rotate(95grad);transform: rotate(95grad);"></i>
                                            @lang('adminlte::adminlte.registration')
                                        </a>
                                    </li>
                                    <li class="sub_menu">
                                        <a href="{{route('list.objects',['state_id' => 0])}}" target="_blank">
                                            <i class="fa fa-fw  fa-level-up"
                                               style="-webkit-transform: rotate(95grad);-moz-transform: rotate(95grad);-ms-transform: rotate(95grad);-o-transform: rotate(95grad);transform: rotate(95grad);"></i>
                                            @lang('adminlte::adminlte.object.opt_draft')
                                        </a>
                                    </li>
                                    <li class="sub_menu">
                                        <a href="{{route('list.objects',['state_id' => 1])}}" target="_blank">
                                            <i class="fa fa-fw  fa-level-up"
                                               style="-webkit-transform: rotate(95grad);-moz-transform: rotate(95grad);-ms-transform: rotate(95grad);-o-transform: rotate(95grad);transform: rotate(95grad);"></i>
                                            @lang('adminlte::adminlte.object.opt_waiting_rev')
                                        </a>
                                    </li>
                                    <li class="sub_menu">
                                        <a href="{{route('list.objects',['state_id' => 2])}}" target="_blank">
                                            <i class="fa fa-fw  fa-level-up"
                                               style="-webkit-transform: rotate(95grad);-moz-transform: rotate(95grad);-ms-transform: rotate(95grad);-o-transform: rotate(95grad);transform: rotate(95grad);"></i>
                                            @lang('adminlte::adminlte.object.opt_approved')
                                        </a>
                                    </li>
                                    <li class="sub_menu">
                                        <a href="{{route('list.objects',['state_id' => 3])}}" target="_blank">
                                            <i class="fa fa-fw  fa-level-up"
                                               style="-webkit-transform: rotate(95grad);-moz-transform: rotate(95grad);-ms-transform: rotate(95grad);-o-transform: rotate(95grad);transform: rotate(95grad);"></i>
                                            @lang('adminlte::adminlte.object.opt_rej_adj')
                                        </a>
                                    </li>
                                    <li class="sub_menu">
                                        <a href="{{route('list.objects',['state_id' => 4])}}" target="_blank">
                                            <i class="fa fa-fw  fa-level-up"
                                               style="-webkit-transform: rotate(95grad);-moz-transform: rotate(95grad);-ms-transform: rotate(95grad);-o-transform: rotate(95grad);transform: rotate(95grad);"></i>
                                            @lang('adminlte::adminlte.object.opt_rej_ever')
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        @endif

                </ul>
            </div>


            <!-- Left Side-->

            <div class="col-md-6">

                @if(Auth::check())
                    @if(Auth::user()->isAdmin())
                        <ul>
                            <li class="menu">
                                <a href="{{route('dashboard.user.index')}}">
                                    <i class="fa fa-fw  fa-folder-open-o"></i>
                                    @lang('adminlte::adminlte.user')
                                </a>

                                <ul style="margin-top: -2px">
                                    <li class="sub_menu">
                                        <a href="{{route('dashboard.user.create')}}" target="_blank">
                                            <i class="fa fa-fw  fa-level-up"
                                               style="-webkit-transform: rotate(95grad);-moz-transform: rotate(95grad);-ms-transform: rotate(95grad);-o-transform: rotate(95grad);transform: rotate(95grad);"></i>
                                            @lang('adminlte::adminlte.registration')
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    @endif
                @endif


                <ul>
                    <li class="menu">
                        <a href="{{route('map.index')}}">
                            <i class="fa fa-fw  fa-map-o"></i>
                            @lang('footer.map')
                        </a>
                    </li>
                </ul>
                <ul>
                    <li class="menu">
                        <a href="{{route('accessibility.index')}}">
                            <i class="fa fa-fw  fa-wheelchair"></i>
                            @lang('footer.accessibility')
                        </a>
                    </li>
                </ul>
                <ul>
                    <li class="menu">
                        <a href="mailto:carlamarina@gmail.com?subject=Contato%20Acacia">
                            <i class="fa fa-fw  fa-envelope-o"></i>
                            Contato
                        </a>
                    </li>
                </ul>

            </div>

        </div>

    </div>

    <link rel="stylesheet" href="{{ asset('css/map.css') }}">


    {{--.sitemap-menu li li > a::before {
          border-left: 2px solid #6b8894;
          border-top: 2px solid green;
          }--}}


@endsection
