@extends('layouts.app')
@section('content')

    <div style="height: 8em"></div>

    <div align="center">
        <a title="ROAC" href="{{route('dashboard')}}" target="_blank">
            <img  src="{{ url('img/logo_small.png') }}" alt="ROAC"/>
        </a>
        <a title="ACACIA" href="https://acacia.digital/pt/" target="_blank">
            <img  style="height: 4em" src="{{ url('img/logo_acacia.png') }}" alt="ACACIA"/>
        </a>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <form method="GET" action="{{ route('public.basic.search') }}">
                        <div class="input-group input-group-sm col-10 offset-4">
                            <input type="text" class="form-control" name="search">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-info btn-flat">Go!</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




    <div style="height: 16em"></div>
@endsection

