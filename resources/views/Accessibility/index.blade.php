@extends('layouts.app')
{{--@extends('adminlte::page')--}}

@section('content_header')
    <h1 align="center">@lang('footer.accessibility')</h1>
@endsection

@section('content')
    <div class="box box-solid" style="margin-left: 10%; margin-right: 10%; font-size: 1.25rem">
        <div align="center">
            <a title="ROAC" href="{{route('dashboard')}}" target="_blank">
                <img src="{{ url('img/logo_small.png') }}" alt="ROAC"/>
            </a>
            <a title="ACACIA" href="https://acacia.digital/pt/" target="_blank">
                <img style="height: 4em" src="{{ url('img/logo_acacia.png') }}" alt="ACACIA"/>
            </a>
        </div>

        <div style="text-align: justify; margin-top: 2rem">
            <p>
                @lang('adminlte::strings.page_of_accessibility.p1')
            </p>
            <p>
                @lang('adminlte::strings.page_of_accessibility.p2')
            </p>
            <p>
                @lang('adminlte::strings.page_of_accessibility.p3')
            </p>

            <p>
                @lang('adminlte::strings.page_of_accessibility.p4')
            </p>

            <ul>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p5')
                    </p>
                </li>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p6')
                    </p>
                </li>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p7')
                    </p>
                </li>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p8')
                    </p>
                </li>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p9')
                    </p>
                </li>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p10')
                    </p>
                </li>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p11')
                    </p>
                </li>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p12')
                    </p>
                </li>
            </ul>

            <p>@lang('adminlte::strings.page_of_accessibility.p13')</p>
            <ul>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p14')
                    </p>
                </li>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p15')
                    </p>
                </li>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p16')
                    </p>
                </li>
                <li>
                    <p>
                        @lang('adminlte::strings.page_of_accessibility.p17')
                    </p>
                </li>
            </ul>
        </div>

    </div>

@endsection
