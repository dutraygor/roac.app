<!--Accessibility-->
<div id="acessibilidade" class="hidden-xs">
    <div class="container">
        <div class="row">
            <ul class="nav nav-pills">
                <li>
                    <a accesskey="w" href="#menu" id="link-nav"> @lang('accessibility.go_to_menu')
                        <span class="button_accessibility">[ w ]</span></a>
                </li>
                <li>
                    <a accesskey="t" href="#container" id="link-nav">@lang('accessibility.go_to_content')
                        <span class="button_accessibility">[ t ]</span></a>
                </li>
                <li>
                    <a accesskey="a" href="#footer" id="link-nav">@lang('accessibility.go_to_footer')
                        <span class="button_accessibility">[ a ]</span></a>
                </li>
                <li>
                    <a accesskey="q" href="{{route('map.index')}}"
                       id="link-nav">@lang('accessibility.go_to_map')<span class="button_accessibility">[ q ]</span></a>
                </li>
                <li>
                    <a accesskey="s" class="ativa-contraste" href="#" id="is-normal-contrast"
                       onclick="window.toggleContrast(); location.reload()"
                       onkeydown="window.toggleContrast()">@lang('accessibility.high_contrast')<span
                                class="button_accessibility">[ s ]</span></a>
                </li>
                <li>
                    <a accesskey="0" href="#" class="res-font" onclick='textoNormal()'>A</a>
                </li>
                <li>
                    <a accesskey="-" href="#" class="dec-font" onclick='textoMenor()'>A-</a>
                </li>
                <li>
                    <a accesskey="=" href="#" class="inc-font" onclick='textoMaior()'>A+</a>
                </li>

                <!--Plugin of Translation-->
                <li class="text-right pull-right" style="margin-top: 5px;margin-bottom: 5px">
                    <div id="google_translate_element">

                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({
                                    pageLanguage: 'es',
                                    includedLanguages: 'de,en,fr,it,pt,ru',
                                    layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                                    multilanguagePage: true
                                }, 'google_translate_element');
                            }
                        </script>
                        <script type="text/javascript"
                                src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">
                        </script>

                    </div>
                </li>

            </ul>

        </div>
    </div>
</div>


<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/high-contrast.js') }}"></script>


<script>
    function textoMaior() {
        var size = $("body").css('font-size');
        size = size.replace('px', '');
        size = parseInt(size) + 1.4;
        var h1 = size + 22;
        var h2 = size + 16;
        var h3 = size + 10;
        var h4 = size + 4;
        var h5 = size;
        var h6 = size - 2;
        var title = size + 70;
        $("body, th, td, #footer a, #footer p, #map a, strong").animate({'font-size': size + 'px'});
        $(".btn").animate({'font-size': size + 'px'});
        $(".h1, h1").animate({'font-size': h1 + 'px'});
        $(".h2, h2").animate({'font-size': h2 + 'px'});
        $(".h3, h3").animate({'font-size': h3 + 'px'});
        $(".h4, h4").animate({'font-size': h4 + 'px'});
        $(".h5, h5").animate({'font-size': h5 + 'px'});
        $(".h6, h6").animate({'font-size': h6 + 'px'});
        $(".title").animate({'font-size': title + 'px'});
    }
</script>

<script>
    function textoMenor() {
        var size = $("body").css('font-size');
        size = size.replace('px', '');
        size = parseInt(size) - 1.4;
        var h1 = size + 22;
        var h2 = size + 16;
        var h3 = size + 10;
        var h4 = size + 4;
        var h5 = size;
        var h6 = size - 2;
        var title = size + 50;
        $("body, th, td, #footer a, #footer p, #map a, strong").animate({'font-size': size + 'px'});
        $(".btn").animate({'font-size': size + 'px'});
        $(".h1, h1").animate({'font-size': h1 + 'px'});
        $(".h2, h2").animate({'font-size': h2 + 'px'});
        $(".h3, h3").animate({'font-size': h3 + 'px'});
        $(".h4, h4").animate({'font-size': h4 + 'px'});
        $(".h5, h5").animate({'font-size': h5 + 'px'});
        $(".h6, h6").animate({'font-size': h6 + 'px'});
        $(".title").animate({'font-size': title + 'px'});
    }
</script>

<script>
    function textoNormal() {
        $("body, th, td, #footer a, #footer p, #map a, strong").animate({'font-size': '14px'});
        $(".btn").animate({'font-size': '14px'});
        $(".h1, h1").animate({'font-size': '24px'});
        $(".h2, h2").animate({'font-size': '22px'});
        $(".h3, h3").animate({'font-size': '20px'});
        $(".h4, h4").animate({'font-size': '18px'});
        $(".h5, h5").animate({'font-size': '12px'});
        $(".h6, h6").animate({'font-size': '10px'});
        $(".title").animate({'font-size': '84px'});
    }
</script>

<!-- Styles -->
<link rel="stylesheet" href="{{ asset('css/contrast.css') }}">

<style type="text/css">
    #acessibilidade {
        line-height: 0;
    }

    #acessibilidade a {
        line-height: 10px;
    }

    #acessibilidade ul > li {
        margin-bottom: 2px;
    }

    #acessibilidade .button_accessibility {
        background: #2ab27b;
        border-radius: 2px;
        padding: 2px 4px;
        margin-left: 4px;
        color: #fff;
    }
</style>
