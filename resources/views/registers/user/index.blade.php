@extends('adminlte::page')

@section('title', config('app.name') )

@section('content_header')
    <h1>{{trans('adminlte::adminlte.user')}} {{trans('adminlte::adminlte.registration')}}
        <a class="btn btn-success pull-right" href="{{route('dashboard.user.create')}}">
            <i class="fa fa-fw fa-plus-circle"></i>
            @lang('adminlte::adminlte.add')
        </a>
    </h1>

@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">

            <div class="box-body">


                @if(!$users->isEmpty())
                    @foreach($users as $user)
                        <p>{{$user->name}}</p>
                    @endforeach
                @else

                    <div class="alert alert-warning">Nenhum Usuario encontrado</div>

                @endif


            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
    </div>
@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
