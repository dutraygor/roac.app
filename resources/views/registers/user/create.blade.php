@extends('adminlte::page')

{{--@push('css')--}}
{{--<link rel="stylesheet" href="{{asset('css/select2.min.css')}}">--}}
{{--@endpush--}}

@section('content_header')


    <h1>{{trans('adminlte::adminlte.user')}} {{trans('adminlte::adminlte.registration')}}</h1>

@endsection

@section('content')

    <div class="container">
        {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
        {{--<div class="panel panel-default">--}}
        <form method="POST" action="{{ route('dashboard.user.store') }}" role="form">
            <div class="box-body">
                {{ csrf_field() }}
                <div class="row">
                    <div class="form-group col-xs-12 col-md-6 pull-left">
                        <label>@lang('adminlte::adminlte.name')</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                </div>

                {{--<div id="datemask" class="row">--}}
                    {{--<div class="form-group col-xs-12 col-md-6 pull-left">--}}
                        {{--<label>@lang('adminlte::adminlte.birth') (dd/mm/yyyy)</label>--}}
                        {{--<input type="text" name="birth_date" class="form-control">--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="row">--}}
                    {{--<div class="form-group col-xs-12 col-md-6 pull-left">--}}
                        {{--<label>@lang('adminlte::adminlte.gender')</label>--}}
                        {{--<select name="gender" class="form-control">--}}
                            {{--<option value="Male">@lang('adminlte::adminlte.male')</option>--}}
                            {{--<option value="Female">@lang('adminlte::adminlte.female')</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="row">
                    <div class="form-group col-xs-12 col-md-6 pull-left">
                        <label>@lang('adminlte::adminlte.email')</label>
                        <input type="email" name="email" class="form-control">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-xs-12 col-md-6 pull-left">
                        <label>@lang('adminlte::adminlte.password')</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-xs-12 col-md-6 pull-left">
                        <label>@lang('adminlte::adminlte.role')</label>
                        <select name="role" class="form-control">
                            <option value="{{ \App\Models\User::ADMIN }}">@lang('adminlte::adminlte.admin')</option>
                            <option value="{{ \App\Models\User::COLLABORATOR }}">@lang('adminlte::adminlte.collaborator')</option>
                        </select>
                    </div>
                </div>
                <div class="row col-xs-12 col-md-6 pull-left">
                    <input type="submit" value="@lang('adminlte::adminlte.create')"
                           class="btn btn-success btn-block">
                </div>
            </div>
        </form>
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>

    @push('js')
        <script src="{{asset('js/select2.min.js')}}"></script>
    @endpush
    @push('js')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
        <script>
            $(function () {
                //Initialize Select2 Elements
                $('.select2').select2();

                //Datemask dd/mm/yyyy
                $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
                //Datemask2 mm/dd/yyyy
                $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
                //Money Euro
                $('[data-mask]').inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({
                    timePicker: true,
                    timePickerIncrement: 30,
                    format: 'MM/DD/YYYY h:mm A'
                });
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                    {
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                        startDate: moment().subtract(29, 'days'),
                        endDate: moment()
                    },
                    function (start, end) {
                        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                    }
                );

                //Date picker
                $('#datepicker').datepicker({
                    autoclose: true
                });

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });

                //Colorpicker
                $('.my-colorpicker1').colorpicker();
                //color picker with addon
                $('.my-colorpicker2').colorpicker();

                //Timepicker
                $('.timepicker').timepicker({
                    showInputs: false
                })
            })
        </script>
    @endpush
@endsection

