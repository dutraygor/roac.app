@extends('adminlte::page')

@section('title', config('app.name') )

@section('content_header')
    <h1>@lang('adminlte::adminlte.my_objects')</h1>
@stop

@section('content')

    <div class="margin">
        <div class="row">
           {{-- <h3 style="margin: 0.5rem" class="pull-left">Meus Objetos</h3>--}}
            <a style="margin-left: 0.5rem" title="@lang('adminlte::adminlte.object.opt_rej_ever')" class="btn btn-danger pull-right"
               href="{{route('list.objects',['state_id' => 4])}}">
                <i style="margin-right: 2px" class="fa fa-thumbs-o-down "></i>@lang('adminlte::adminlte.object.opt_rej_ever')
            </a>
            <a style="margin-left: 0.5rem" title="@lang('adminlte::adminlte.object.opt_rej_adj')" class="btn btn-warning pull-right"
               href="{{route('list.objects',['state_id' => 3])}}">
                <i style="margin-right: 2px" class="fa fa-history "></i>@lang('adminlte::adminlte.object.opt_rej_adj')
            </a>
            <a style="margin-left: 0.5rem" title="@lang('adminlte::adminlte.object.opt_waiting_rev')" class="btn btn-info pull-right"
               href="{{route('list.objects',['state_id' => 1])}}">
                <i style="margin-right: 2px" class="fa fa-spinner"></i>@lang('adminlte::adminlte.object.opt_waiting_rev')
            </a>
            <a title="@lang('adminlte::adminlte.object.opt_approved')" class="btn btn-success pull-right"
               href="{{route('list.objects',['state_id' => 2])}}">
                <i style="margin-right: 2px" class="fa fa-check"></i>@lang('adminlte::adminlte.object.opt_approved')
            </a>


            <a style="margin-right: 0.5rem" title="@lang('adminlte::adminlte.add')" class="btn btn-success pull-left" href="{{route('dashboard.object.create')}}">
                <i class="fa fa-fw fa-plus-circle"></i>
                @lang('adminlte::adminlte.add')
            </a>

            <a title="@lang('adminlte::adminlte.object.opt_draft')" class="btn bg-olive pull-left"
               href="{{route('list.objects',['state_id' => 0])}}">
                <i class="fa fa-fw fa-file-text-o"></i>
                @lang('adminlte::adminlte.object.opt_draft')
            </a>

        </div>
    </div>

    <div class="box">

        <div class="box-header with-border">

            <div class="box-body">

                @if(!$objects->isEmpty())

                    <div class="table-responsive">

                        <table class="table">

                            <thead>
                            <tr>
                                <th> @lang('adminlte::adminlte.name')</th>
                                <th>@lang('adminlte::adminlte.status')</th>
                                <th>@lang('adminlte::adminlte.created')</th>
                                <th>@lang('adminlte::adminlte.actions')</th>

                            </tr>
                            </thead>

                            <tbody>
                            @foreach($objects as $object)
                                <tr>
                                    <td>{{ $object->title }}</td>
                                    <td>{{ $object->description }}</td>
                                    <td><?php echo date('d/m/Y - H:i:s', strtotime( $object->los->order )); ?></td>
                                    <td>

                                        <a style="margin-right: 1rem"
                                           href="{{ route('opt.view', ['object_id' => $object->los->lobject_id]) }}"
                                           title="@lang('adminlte::adminlte.view')" class="btn btn-default"><i class="fa fa-fw fa-eye"></i>
                                        </a>

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>


                        </table>
                    </div>


                @else

                    <div class="callout callout-warning">
                        <h4>@lang('adminlte::adminlte.object.none')</h4>
                    </div>

                @endif
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
    </div>
    {{--
        @push('menu-items')
            @each('adminlte::partials.menu-item', config('menu'), 'item')
        @endpush--}}
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
