@extends('adminlte::page')

@section('title', config('app.name') )

@section('content_header')
    <h1>@lang('adminlte::adminlte.object.opt_rej_ever')</h1>

@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">

            <div class="box-body">

                @if(!$objects->isEmpty())

                    <div class="table-responsive">

                        <table class="table">

                            <thead>
                            <tr>


                                <th>@lang('adminlte::adminlte.name')</th>
                                <th>@lang('adminlte::adminlte.format')</th>
                                <th>@lang('adminlte::adminlte.size')</th>
                                <th>@lang('adminlte::adminlte.motivation')</th>
                                <th>@lang('adminlte::adminlte.actions')</th>

                            </tr>
                            </thead>

                            <tbody>
                            @foreach($objects as $object)

                                <tr>
                                    <td>{{ $object->title }}</td>
                                    <td>{{ $object->format }}</td>
                                    <td>{{ $object->size }}</td>
                                    {{--<td>{{ $object->order }}</td>--}}
                                    <td style="width: 30%">{{ $object->reason_of_rejection }}</td>
                                    <td>
                                        <a href="{{ route('opt.view', ['object_id' => $object->los->lobject_id]) }}"
                                           title="@lang('adminlte::adminlte.view')" class="btn btn-default"><i class="fa fa-fw fa-eye"></i>
                                        </a>
                                        {{--<a title="Ver motivação" class="btn btn-default" style="margin-left: 1rem"
                                           data-toggle="modal" data-target="#modal-obj-rej">
                                            <i class="fa fa-fw fa-commenting-o"></i>
                                        </a>--}}

                                    </td>
                                </tr>


                                {{--Reason for rejection of Object--}}
                               {{-- <div class="modal fade in" id="modal-obj-rej" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                                <h4 class="modal-title">Objeto Rejeitado</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{$object->title}}</p>
                                                <p>{{$object->reason_of_rejection}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>--}}



                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else

                    <div class="callout callout-warning">
                        <h4>@lang('adminlte::adminlte.object.none')</h4>
                    </div>

                @endif

            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
    </div>



@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
