@extends('adminlte::page')

@section('title', config('app.name') )

@section('content_header')
    <h1>@lang('adminlte::adminlte.object.opt_rej_adj')</h1>

@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">

            <div class="box-body">

                @if(!$objects->isEmpty())

                    <div class="table-responsive">

                        <table class="table">

                            <thead>
                            <tr>

                                <th>@lang('adminlte::adminlte.name')</th>
                                <th>@lang('adminlte::adminlte.format')</th>
                                <th>@lang('adminlte::adminlte.size')</th>
                                <th>@lang('adminlte::adminlte.latest_review')</th>
                                <th>@lang('adminlte::adminlte.actions')</th>


                            </tr>
                            </thead>

                            <tbody>
                            @foreach($objects as $object)
                                <tr>
                                    <td>{{ $object->title }}</td>
                                    <td>{{ $object->format }}</td>
                                    <td>{{ $object->size }}</td>
                                    <td><?php echo date('d/m/Y - H:i:s', strtotime( $object->los->order )); ?></td>
                                    <td>

                                        <a href="{{ route('opt.view', ['object_id' => $object->los->lobject_id]) }}"
                                           title="@lang('adminlte::adminlte.view')" class="btn btn-default" style="margin-left: 1rem">
                                            <i
                                                    class="fa fa-fw fa-eye"></i>
                                        </a>
                                        <a href="{{route('opt.pend', ['object_id' => $object->los->lobject_id]) }}"
                                           title="@lang('adminlte::adminlte.view_pendency')" class="btn btn-default" style="margin-left: 1rem">
                                            <i class="fa fa-fw fa-commenting-o"></i>
                                        </a>

                                        <a href="{{ route('opt.edit', ['object_id' => $object->los->lobject_id]) }}"
                                           title="@lang('adminlte::adminlte.edit')" class="btn btn-default" style="margin-left: 1rem">
                                            <i class="fa fa-fw fa-pencil"></i>
                                        </a>


                                    </td>

                                </tr>

                            @endforeach
                            </tbody>


                        </table>
                    </div>
                @else

                    <div class="callout callout-warning">
                        <h4>@lang('adminlte::adminlte.object.none')</h4>
                    </div>

                @endif

            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
    </div>
    {{--
        @push('menu-items')
            @each('adminlte::partials.menu-item', config('menu'), 'item')
        @endpush--}}
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
