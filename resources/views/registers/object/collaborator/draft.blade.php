@extends('adminlte::page')

@section('title', config('app.name') )

@section('content_header')
    <h1>@lang('adminlte::adminlte.object.opt_draft')</h1>

@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">

            <div class="box-body">

                @if(!$objects->isEmpty())

                    <div class="table-responsive">

                        <table class="table">

                            <thead>
                            <tr>
                                <th>@lang('adminlte::adminlte.name')</th>
                                <th>@lang('adminlte::adminlte.format')</th>
                                <th>@lang('adminlte::adminlte.size')</th>
                                <th>@lang('adminlte::adminlte.created')</th>
                                <th>@lang('adminlte::adminlte.actions')</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($objects as $object)
                                <tr>
                                    <td>{{ $object->title }}</td>
                                    <td>{{ $object->format }}</td>
                                    <td>{{ $object->size }}</td>
                                    <td><?php echo date('d/m/Y - H:i:s', strtotime( $object->los->order )); ?></td>
                                    <td>
                                        <a href="{{ route('opt.view', ['object_id' => $object->los->lobject_id]) }}"
                                           title="@lang('adminlte::adminlte.view')" class="btn btn-default"><i class="fa fa-fw fa-eye"></i>
                                        </a>
                                        <a href="{{ route('opt.edit', ['object_id' => $object->los->lobject_id]) }}"
                                           title="@lang('adminlte::adminlte.edit')" class="btn btn-default" style="margin-left: 1rem">
                                            <i class="fa fa-fw fa-pencil"></i>
                                        </a>
                                       {{-- <a title="Excluir" class="btn btn-danger" style="margin-left: 1rem"
                                           data-toggle="modal" data-target="#modal-del-def">
                                            <i class="fa fa-fw fa-trash-o"></i>
                                        </a>--}}
                                        <a href="{{route('opt.delete_ever',['object_id' => $object->los->lobject_id]) }}"
                                           title="@lang('adminlte::adminlte.delete_ever')" class="btn btn-danger" style="margin-left: 1rem">
                                            <i class="fa fa-fw fa-trash-o"></i>
                                        </a>

                                    </td>
                                </tr>

                                {{--Excluir Definitivamente o Objeto--}}
                                {{--<div class="modal modal-danger fade in" id="modal-del-def" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                                <h4 class="modal-title">Excluir Objeto</h4>
                                            </div>
                                            <div class="modal-body">

                                                <p>Ao clicar em excluir, você estará concordando em deletar o
                                                    objeto "{{$object->title}}" definitivamente da Base de Dados do ROAC.</p>
                                                <p>Id: {{$object->los->lobject_id}}</p>

                                            </div>
                                            <div class="modal-footer">
                                                --}}{{--Delte Object--}}{{--

                                                <form id="formDeleteObject"
                                                      action="{{ route('object.destroy', [$object->los->lobject_id]) }}"
                                                      method="POST">

                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}

                                                    <br>

                                                    <button type="button" class="btn btn-outline pull-left"
                                                            data-dismiss="modal">Fechar
                                                    </button>

                                                    <input type="submit" title="Excluir"
                                                           class="btn btn-outline pull-right"
                                                           style="margin-left: 1rem"
                                                           value="Excluir">

                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>--}}


                            @endforeach
                            </tbody>


                        </table>
                    </div>
                @else

                    <div class="callout callout-warning">
                        <h4>@lang('adminlte::adminlte.object.none')</h4>
                    </div>

                @endif
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
    </div>
    {{--
        @push('menu-items')
            @each('adminlte::partials.menu-item', config('menu'), 'item')
        @endpush--}}
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
