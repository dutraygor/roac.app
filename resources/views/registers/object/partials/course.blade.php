{{--<div class="row">--}}
{{--<div class="form-group col-xs-12 col-md-6 pull-left">--}}
{{--<label>@lang('adminlte::adminlte.course')</label>--}}
{{--<div id="course" class="input-group">--}}
    {{--<select name="course" class="form-control">--}}
        {{--<option value="">@lang('adminlte::adminlte.choose_a_course')</option>--}}
        {{--@foreach(\App\Models\Course::all() as $course)--}}
            {{--<option value="{{$course->id}}">{{$course->name}}</option>--}}
        {{--@endforeach--}}
    {{--</select>--}}

    {{--<div id="btn-course" class="input-group-btn">--}}
        {{--<a class="btn btn-default">--}}
            {{--<i class="fa fa-fw fa-plus-circle"></i>--}}
        {{--</a>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group">
    <label>@lang('adminlte::adminlte.course')</label>
    <select name="course[]" multiple class="form-control select2" style="width: 100%" required>
        @foreach(\App\Models\Course::all() as $course)
            <option value="{{$course->id}}">{{$course->name}}</option>
        @endforeach
    </select>
</div>