{{--<div class="row">--}}

{{--<div class="form-group col-xs-12 col-md-6 pull-left">--}}
{{--<label>@lang('adminlte::adminlte.disability')</label>--}}
{{--<div class="input-group">--}}
    {{--<select name="disability" class="form-control">--}}
        {{--<option value="">@lang('adminlte::adminlte.choose_a_disability')</option>--}}
        {{--@foreach(\App\Models\Disability::all() as $disability)--}}
            {{--<option value="{{$disability->id}}">{{$disability->name}}</option>--}}
        {{--@endforeach--}}
    {{--</select>--}}

    {{--<div class="input-group-btn">--}}
        {{--<a class="btn btn-default">--}}
            {{--<i class="fa fa-fw fa-plus-circle"></i>--}}
        {{--</a>--}}
    {{--</div>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}

<div class="form-group">
    <label>@lang('adminlte::adminlte.disability')</label>
    <select name="disability[]" multiple class="form-control select2" style="width: 100%"
            required>
        @foreach(\App\Models\Disability::all() as $disability)
            <option value="{{$disability->id}}">{{$disability->name}}</option>
        @endforeach
    </select>
</div>