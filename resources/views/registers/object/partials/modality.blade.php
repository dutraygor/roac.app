{{--<div class="row">--}}
{{--<div class="form-group col-xs-12 col-md-6 pull-left">--}}

<label>@lang('adminlte::adminlte.modality')</label>
<div class="input-group">
    <select name="modality" class="form-control">
        <option value=""> @lang('adminlte::adminlte.choose_a_modality') </option>
        @foreach(\App\Models\Modality::all() as $modality)
            <option value="{{$modality->id}}">{{$modality->name}}</option>
        @endforeach
    </select>
</div>

{{--<div class="form-group">--}}
    {{--<label>@lang('adminlte::adminlte.modality')</label>--}}
    {{--<div class="input-group">--}}
        {{--<select name="modality" class="form-control">--}}
            {{--@foreach(\App\Models\Modality::all() as $modality)--}}
                {{--<option value="{{$modality->id}}">{{$modality->name}}</option>--}}
            {{--@endforeach--}}
        {{--</select>--}}
    {{--</div>--}}
{{--</div>--}}