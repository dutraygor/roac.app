{{--<div class="row">--}}
{{--<div class="form-group col-xs-12 col-md-6 pull-left">--}}

{{--<label>@lang('adminlte::adminlte.subject')</label>--}}
{{--<div class="input-group">--}}
    {{--<select name="subject" class="form-control">--}}
        {{--<option value="">@lang('adminlte::adminlte.choose_a_subject') </option>--}}
        {{--@foreach(\App\Models\Subject::all() as $subject)--}}
            {{--<option value="{{$subject->id}}">{{$subject->name}}</option>--}}
        {{--@endforeach--}}
    {{--</select>--}}

    {{--<div class="input-group-btn">--}}
        {{--<a class="btn btn-default">--}}
            {{--<i class="fa fa-fw fa-plus-circle"></i>--}}
        {{--</a>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group">
    <label>@lang('adminlte::adminlte.subject')</label>
    <select name="subject[]" multiple class="form-control select2" style="width: 100%" required>
        @foreach(\App\Models\Subject::all() as $subject)
            <option value="{{$subject->id}}">{{$subject->name}}</option>
        @endforeach
    </select>
</div>