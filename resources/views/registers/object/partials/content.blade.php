{{--<div class="row">--}}
{{--<div class="form-group col-xs-12 col-md-6 pull-left">--}}

{{--<label>@lang('adminlte::adminlte.content')</label>--}}
{{--<div id="content" class="input_fields_wrap input-group">--}}
    {{--<select name="content" class="form-control">--}}
        {{--<option value="">@lang('adminlte::adminlte.choose_a_content')</option>--}}
        {{--@foreach(\App\Models\Content::all() as $content)--}}
            {{--<option value="{{$content->id}}">{{$content->name}}</option>--}}
        {{--@endforeach--}}
    {{--</select>--}}

    {{--<div id="btn-content" class="input-group-btn">--}}
        {{--<a class=" btn btn-default">--}}
            {{--<i class="fa fa-fw fa-plus-circle"></i>--}}
        {{--</a>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group">
    <label>@lang('adminlte::adminlte.content')</label>
    <select name="content[]" multiple class="form-control select2" style="width: 100%" required>
        @foreach(\App\Models\Content::all() as $content)
            <option value="{{$content->id}}">{{$content->name}}</option>
        @endforeach
    </select>
</div>