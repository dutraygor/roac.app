{{--<div class="row">--}}
{{--<div class="form-group col-xs-12 col-md-6 pull-left">--}}

{{--<label>@lang('adminlte::adminlte.educ_level')</label>--}}

{{--<div class="input-group">--}}
    {{--<select name="educ_level" class="form-control">--}}
        {{--<option value="">@lang('adminlte::adminlte.choose_a_educational_level')</option>--}}
        {{--@foreach(\App\Models\EducationalLevel::all() as $educLevel)--}}
            {{--<option value="{{$educLevel->id}}">{{$educLevel->level}}</option>--}}
        {{--@endforeach--}}
    {{--</select>--}}
    {{--<div class="input-group-btn">--}}
        {{--<a class="btn btn-default">--}}
            {{--<i class="fa fa-fw fa-plus-circle"></i>--}}
        {{--</a>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group">
    <label>@lang('adminlte::adminlte.educ_level')</label>

    <select name="educ_level[]" multiple class="form-control select2"
            style="width: 100%" required>
        @foreach(\App\Models\EducationalLevel::all() as $educLevel)
            <option value="{{$educLevel->id}}">{{$educLevel->level}}</option>
        @endforeach
    </select>

</div>