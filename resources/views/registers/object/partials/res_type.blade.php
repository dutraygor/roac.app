{{--<div class="row">--}}
{{--<div class="form-group col-xs-12 col-md-6 pull-left">--}}

{{--<label>@lang('adminlte::adminlte.resource_type')</label>--}}
{{--<div id="res" class="input-group">--}}
    {{--<select name="res_type" class="form-control">--}}
        {{--<option value="">@lang('adminlte::adminlte.choose_a_resource_type') </option>--}}
        {{--@foreach(\App\Models\ResourceType::all() as $restype)--}}
            {{--<option value="{{$restype->id}}">{{$restype->name}}</option>--}}
        {{--@endforeach--}}
    {{--</select>--}}
{{--</div>--}}

<div class="form-group">
    <label>@lang('adminlte::adminlte.resource_type')</label>
    <div class="input-group">
        <select name="res_type" class="form-control">
            <option value=""></option>
            @foreach(\App\Models\ResourceType::all() as $restype)
                <option value="{{$restype->id}}">{{$restype->name}}</option>
            @endforeach
        </select>
    </div>
</div>