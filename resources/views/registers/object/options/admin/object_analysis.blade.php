@extends('adminlte::page')

@section('content_header')
    <h1>@lang('adminlte::adminlte.object_analysis')</h1>
@endsection

@section('content')

    @include('registers.object.options.both.view_object_partial')

    <div align="center">
        <a style="margin: 1em; width: 20em" class="btn btn-warning" data-toggle="modal"
           data-target="#modal-rej-adj">@lang('adminlte::adminlte.rej_to_analysis')</a>

        <a style="margin: 1em; width: 20em" class="btn  btn-danger" data-toggle="modal"
           data-target="#modal-rej-ever">@lang('adminlte::adminlte.rej_def')</a>

        <a style="margin: 1em; width: 20em" href="{{route('opt.to_approve',['object_id' => $los->lobject_id])}}"
           class="btn btn-success">@lang('adminlte::adminlte.approve')</a>
    </div>



    {{--Rejeitar Definitivamente--}}
    <form action="{{route('opt.rej_def')}}" method="POST" role="form">

        {{ method_field('POST') }}
        {{csrf_field()}}

        <div class="modal fade in" id="modal-rej-ever" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@lang('adminlte::adminlte.rej_def')</h4>
                    </div>
                    <div class="modal-body">


                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">{{$object->title}}</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->


                            <div class="box-body">
                                <div class="form-group" style="margin-left: 8px;margin-right: 8px">
                                    <label>@lang('adminlte::adminlte.motivation')</label>
                                    <textarea class="form-control" rows="4" name="reason"
                                              placeholder="Deja aquí tu Motivación" required
                                              style="resize: none"></textarea>
                                </div>
                                {{--pega o id do objeto--}}
                                <div class="form-group">
                                    <label style="display: none">Id do Objeto</label>
                                    <input style="display: none" name="object_id" type="text" class="form-control"
                                           value="{{$los->lobject_id}}" readonly>
                                </div>

                            </div>
                            <!-- /.box-body -->

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                                    @lang('adminlte::adminlte.cancel')
                                </button>
                                <input type="submit" value="@lang('adminlte::adminlte.save')"
                                       class="btn btn-primary pull-right">
                            </div>


                        </div>

                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

        </div>

    </form>



    {{--Rejeitar para ajustes--}}
    <form action="{{route('opt.rej_adj')}}" method="POST" role="form">

        {{ method_field('POST') }}
        {{csrf_field()}}

        <div class="modal fade in" id="modal-rej-adj" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">@lang('adminlte::adminlte.rej_to_analysis')</h4>
                    </div>
                    <div class="modal-body">


                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">{{$object->title}}</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->


                            <div class="box-body">
                                <div class="form-group" style="margin-left: 8px;margin-right: 8px">
                                    <div class="form-group" style="margin-left: 8px;margin-right: 8px">
                                        <label>@lang('adminlte::adminlte.orientation')</label>
                                        <textarea class="form-control" rows="4" name="adjustment"
                                                  placeholder="Deja aquí tu orientación" required
                                                  style="resize: none"></textarea>
                                    </div>
                                </div>
                                {{--pega o id do objeto--}}
                                <div class="form-group">
                                    <label style="display: none">Id do Objeto</label>
                                    <input style="display: none" name="object_id" type="text" class="form-control"
                                           value="{{$los->lobject_id}}" readonly>
                                </div>

                            </div>
                            <!-- /.box-body -->

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                                    @lang('adminlte::adminlte.cancel')
                                </button>
                                <input type="submit" value="@lang('adminlte::adminlte.save')"
                                       class="btn btn-primary pull-right">
                            </div>


                        </div>

                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

        </div>

    </form>



@endsection



