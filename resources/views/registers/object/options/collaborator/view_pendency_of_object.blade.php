@extends('adminlte::page')

@section('title', config('app.name') )

@section('content_header')
    <h1>@lang('adminlte::adminlte.object.opt_rej_adj')</h1>

@stop

@section('content')

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header" style="border-bottom: #dddddd 1px solid">
                <h2 class="box-title">{{$object->title}}</h2>
                <a title="@lang('adminlte::adminlte.see')" class="btn btn-success pull-right"
                   href="{{asset($object->path)}}" target="_blank">@lang('adminlte::adminlte.see')</a>
            </div>


            <div class="box-body">
                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div style="margin-left: 2rem">
                            <h4>
                                <label>
                                    @lang('adminlte::adminlte.orientation'):
                                </label>
                            </h4>
                            <br>
                            {{$pendency->description}}
                        </div>
                    </div>
                </div>
                <a class="btn btn-outline-primary pull-left" style="margin: 2rem" href="javascript:history.back()">
                    <h4>
                        <i class="fa fa-fw fa-angle-left"></i>
                        @lang('adminlte::adminlte.back')
                    </h4>
                </a>

            </div>
        </div>

        @stop

        @section('adminlte_js')
            <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
