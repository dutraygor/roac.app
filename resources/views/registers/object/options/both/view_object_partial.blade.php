<div class="row" style="margin-bottom: -3rem">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header" style="border-bottom: #dddddd 1px solid">
                <h2 class="box-title">{{$object->title}}</h2>
                <a title="@lang('adminlte::adminlte.see')" class="btn btn-success pull-right"
                   href="{{asset($object->path)}}" target="_blank">@lang('adminlte::adminlte.see')</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example2" class="table table-bordered table-hover dataTable" role="grid"
                                   aria-describedby=" @lang('strings.table_view')"
                                   summary=" @lang('strings.table_view')">
                                <thead>
                                <tr role="row">

                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td style="vertical-align: middle;width: 35%">
                                        <label>@lang('adminlte::adminlte.created_by')</label><br>
                                        {{ $creator->name }}
                                    </td>

                                    <td style="vertical-align: middle;width: 30%">
                                        <label>@lang('adminlte::adminlte.author')</label><br>
                                        {{ $object->author }}
                                    </td>

                                    <td style="vertical-align: middle;width: 35%">
                                        <label>@lang('adminlte::adminlte.description')</label><br>
                                        {{ $object->description }}
                                    </td>
                                </tr>


                                <tr>
                                    <td style="vertical-align: middle;width: 35%">
                                        <label>@lang('adminlte::adminlte.format')</label><br>
                                        {{ $object->format }}
                                    </td>

                                    <td style="vertical-align: middle;width: 30%">
                                        <label>@lang('adminlte::adminlte.size')</label><br>
                                        @if($object->size < 1000000)
                                            {{ number_format($object->size/1000, 0) . 'Kb' }}
                                        @elseif($object->size >= 1000000 and $object->size < 1000000000)
                                            {{ number_format($object->size/1000000, 0) . 'Mb' }}
                                        @else
                                            ERROR
                                        @endif
                                    </td>

                                    <td style="vertical-align: middle;width: 35%">
                                        <label>@lang('adminlte::adminlte.goal')</label><br>
                                        {{ $object->goal }}
                                    </td>
                                </tr>


                                <tr>
                                    <td style="vertical-align: middle;width:  35%">
                                        <label>@lang('adminlte::adminlte.notes')</label><br>
                                        {{ $object->notes }}
                                    </td>

                                    <td style="vertical-align: middle;width: 30%">
                                        <label>@lang('adminlte::adminlte.theme')</label><br>
                                        {{ $object->theme }}
                                    </td>

                                    <td style="vertical-align: middle;width: 35%">
                                        <label>@lang('adminlte::adminlte.modality'):</label><br>
                                        {{ $modality->name }}
                                    </td>
                                </tr>


                                <tr>
                                    <td style="vertical-align: middle;width:  35%">
                                        <label>@lang('adminlte::adminlte.language')</label><br>
                                        {{ $language }}
                                    </td>

                                    <td style="vertical-align: middle;width: 30%">
                                        <label>@lang('adminlte::adminlte.country')</label><br>
                                        {{ $object->country }}
                                    </td>

                                    <td style="vertical-align: middle;width: 35%">
                                        <label>@lang('adminlte::adminlte.keywords')</label><br>
                                        {{ $object->key_words }}
                                    </td>
                                </tr>


                                <tr>
                                    <td style="vertical-align: middle;width:  35%">
                                        <label>@lang('adminlte::adminlte.way'):</label><br>
                                        {{ $object->path }}
                                    </td>

                                    <td style="vertical-align: middle;width: 30%">
                                        <label>Direitos:</label><br>
                                        {{ $object->rights }}
                                    </td>

                                    <td style="vertical-align: middle;width: 35%">
                                        <label>Licensa:</label><br>
                                        {{ $object->license }}
                                    </td>
                                </tr>


                                <tr>
                                    <td style="vertical-align: middle;width:  35%">
                                        <label>@lang('adminlte::adminlte.educ_level')</label><br>

                                        @foreach($educ_level as $el)
                                            <ul>
                                                <li>
                                                    {{$el->level}}
                                                </li>
                                            </ul>
                                        @endforeach

                                    </td>

                                    <td style="vertical-align: middle;width: 30%">
                                        <label>@lang('adminlte::adminlte.course')</label><br>
                                        @foreach($courses as $course)
                                            <ul>
                                                <li>
                                                    {{$course->name}}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </td>

                                    <td style="vertical-align: middle;width: 35%">
                                        <label>@lang('adminlte::adminlte.subject')</label><br>
                                        @foreach($subjects as $subject)
                                            <ul>
                                                <li>
                                                    {{$subject->name}}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </td>
                                </tr>


                                <tr>
                                    <td style="vertical-align: middle;width:  35%">
                                        <label>@lang('adminlte::adminlte.content')</label><br>
                                        @foreach($contents as $content)
                                            <ul>
                                                <li>
                                                    {{$content->name}}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </td>

                                    <td style="vertical-align: middle;width: 30%">
                                        <label>@lang('adminlte::adminlte.disability'):</label><br>
                                        @foreach($disabilities as $disability)
                                            <ul>
                                                <li>
                                                    {{$disability->name}}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </td>

                                    <td style="vertical-align: middle;width: 35%">
                                        <label>@lang('adminlte::adminlte.resource_type')</label><br>
                                        {{$resource_type->name}}
                                    </td>
                                </tr>


                                </tbody>

                            </table>


                        </div>
                    </div>

                </div>

                <a class="btn btn-outline-primary pull-left" style="margin: 2rem" href="javascript:history.back()">
                    <h4>
                        <i class="fa fa-fw fa-angle-left"></i>
                        @lang('adminlte::adminlte.back')
                    </h4>
                </a>

                <br>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->


    </div>
</div>