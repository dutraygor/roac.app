@extends('adminlte::page')


@section('content_header')

    <h1>@lang('adminlte::adminlte.edit')</h1>

@endsection

@section('content')

    <div class="container">

        <form method="POST" action="{{ route('object.update', [$object->id]) }}" enctype="multipart/form-data" id="edit"
              class="form">
            <div class="box-body">

                {{ method_field('PATCH') }}
                {{ csrf_field() }}

                <div class="row">
                    <div class="form-group col-xs-12 col-md-6 pull-left">
                        <label>@lang('adminlte::adminlte.title')</label>

                        <input type="text" name="title" class="form-control"
                               value="{{ old('title') ?? $object->title }}" required>


                        <label>@lang('adminlte::adminlte.author')</label>
                        <input type="text" name="author" class="form-control"
                               value="{{ old('author') ?? $object->author }}" required>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.description')</label>
                            <textarea name="description" class="form-control" rows="4">
                                {{ old('description') ?? $object->description }}
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.goal')</label>
                            <textarea name="goal" class="form-control" rows="4">
                                 {{ old('goal') ?? $object->goal }}
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.notes')</label>
                            <textarea name="note" class="form-control" rows="4">
                                {{ old('note') ?? $object->notes }}
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.theme')</label>
                            <textarea name="theme" class="form-control" rows="4">
                                {{ old('theme') ?? $object->theme }}
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.modality')</label>
                            <div class="input-group">
                                <select name="modality" class="form-control">
                                    <option value="{{$modality_obj->id}}">{{$modality_obj->name}}</option>
                                    @foreach(\App\Models\Modality::all()->except($modality_obj->id) as $modality)
                                        <option value="{{$modality->id}}">{{$modality->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.educ_level')</label>

                            <select name="educ_level[]" multiple class="form-control select2"
                                    style="width: 100%" required>
                                @foreach($educ_level_in as $el)
                                    <option selected value="{{$el->id}}">{{$el->level}}</option>
                                @endforeach

                                @foreach($educ_level_out as $el)
                                    <option value="{{$el->id}}"> {{$el->level}}</option>
                                @endforeach
                            </select>

                        </div>


                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.course')</label>
                            <select name="course[]" multiple class="form-control select2" style="width: 100%" required>
                                @foreach($course_in as $co)
                                    <option selected value="{{$co->id}}">{{$co->name}}</option>
                                @endforeach

                                @foreach($course_out as $co)
                                    <option value="{{$co->id}}">{{$co->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.subject')</label>
                            <select name="subject[]" multiple class="form-control select2" style="width: 100%" required>
                                @foreach($subject_in as $su)
                                    <option selected value="{{$su->id}}">{{$su->name}}</option>
                                @endforeach

                                @foreach($subject_out as $su)
                                    <option value="{{$su->id}}">{{$su->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.content')</label>
                            <select name="content[]" multiple class="form-control select2" style="width: 100%" required>
                                @foreach($content_in as $cont)
                                    <option selected value="{{$cont->id}}">{{$cont->name}}</option>
                                @endforeach

                                @foreach($content_out as $cont)
                                    <option value="{{$cont->id}}">{{$cont->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.disability')</label>
                            <select name="disability[]" multiple class="form-control select2" style="width: 100%"
                                    required>
                                @foreach($disability_in as $dis)
                                    <option selected value="{{$dis->id}}">{{$dis->name}}</option>
                                @endforeach

                                @foreach($disability_out as $dis)
                                    <option value="{{$dis->id}}">{{$dis->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.resource_type')</label>
                            <div class="input-group">
                                <select name="res_type" class="form-control">
                                    <option value="{{$resource_type->id}}">{{$resource_type->name}}</option>
                                    @foreach(\App\Models\ResourceType::all()->except($resource_type->id) as $restype)
                                        <option value="{{$restype->id}}">{{$restype->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label>@lang('adminlte::adminlte.language')</label>
                        <select type="text" name="lang" class="form-control">
                            <option value="{{$object->language}}">{{$object->language}}</option>
                            @foreach($langs as $lang)
                                <option>{{$lang}}</option>
                            @endforeach
                        </select>

                        <label>@lang('adminlte::adminlte.country')</label>
                        <select type="text" name="country" class="form-control">
                            <option value="{{$object->country}}">{{$object->country}}</option>
                            @foreach($countries as $country)
                                <option>{{$country}}</option>
                            @endforeach
                        </select>

                        <label>@lang('adminlte::adminlte.keywords')</label>
                        <input type="text" name="keywords" class="form-control"
                               value="{{ old('keywords') ?? $object->key_words }}" required>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.insert_object')</label>
                            <input title="@lang('adminlte::adminlte.insert_object')" value="put" type="file" name="file" class="form-control">
                            <p class="text-muted"> @lang('adminlte::adminlte.registered_object') <i class="text-yellow"> {{$old_object}}</i></p>
                        </div>


                        <div class="form-group">
                            <div class="radio ">
                                <label>
                                    <input type="radio" name="option" value="3" checked>
                                    <i class="fa fa-fw  fa-save"></i>
                                    @lang('adminlte::adminlte.save_changes_finalize_later')
                                </label>
                            </div>
                            <div class="radio ">
                                <label>
                                    <input type="radio" name="option" value="1">
                                    <i class="fa fa-fw  fa-send-o"></i>
                                    @lang('adminlte::adminlte.end_send_review')
                                </label>
                            </div>
                        </div>

                        <div align="center">
                            <input title=" @lang('adminlte::adminlte.save')" type="submit"
                                   value="@lang('adminlte::adminlte.save')"
                                   class="btn btn-success">


                            <a style="margin-left: 1rem" title="@lang('adminlte::adminlte.cancel')"
                               class="btn btn-danger" href="javascript:history.back()">
                                @lang('adminlte::adminlte.cancel')
                            </a>
                        </div>


                    </div>
                </div>
            </div>
        </form>

    </div>

@endsection