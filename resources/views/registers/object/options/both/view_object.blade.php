@extends('adminlte::page')

@section('content_header')
    <h1>@lang('adminlte::adminlte.details_of_object')</h1>
@endsection

@section('content')

    @include('registers.object.options.both.view_object_partial')


@endsection

