@extends('adminlte::page')
{{--@extends('layouts.app')--}}

@section('content_header')


    <h1>{{trans('adminlte::adminlte.learn_object')}} - {{trans('adminlte::adminlte.registration')}}</h1>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">

@endsection

@section('content')

    <div class="container">
        {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
        {{--<div class="panel panel-default">--}}

        <form method="POST" action="{{ route('dashboard.object.store') }}" role="form" enctype="multipart/form-data">
            <div class="box-body">
                {{ csrf_field() }}
                <div class="row">
                    <div class="form-group col-xs-12 col-md-6 pull-left">
                        <label>@lang('adminlte::adminlte.title')</label>
                        <input type="text" name="title" class="form-control" value="{{old('title')}}">

                        <label>@lang('adminlte::adminlte.author')</label>
                        <input type="text" name="author" class="form-control" value="{{old('author')}}">

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.description')</label>
                            <textarea name="description" class="form-control" rows="4">{{old('description')}}</textarea>
                        </div>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.goal')</label>
                            <textarea name="goal" class="form-control" rows="4"> {{old('goal')}}</textarea>
                        </div>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.notes')</label>
                            <textarea name="note" class="form-control" rows="4">{{old('note')}}</textarea>
                        </div>

                        <div class="form-group">
                            <label>@lang('adminlte::adminlte.theme')</label>
                            <textarea name="theme" class="form-control" rows="4">{{old('theme')}}</textarea>
                        </div>

                        @include('registers.object.partials.modality')
                        @include('registers.object.partials.educ_level')
                        @include('registers.object.partials.course')
                        @include('registers.object.partials.subject')
                        @include('registers.object.partials.content')
                        @include('registers.object.partials.disability')
                        @include('registers.object.partials.res_type')

                        <label>@lang('adminlte::adminlte.language')</label>
                        <select type="text" name="lang" class="form-control">
                            @foreach($langs as $lang)
                                <option>{{$lang}}</option>
                            @endforeach
                        </select>

                        <label>@lang('adminlte::adminlte.country')</label>
                        <select type="text" name="country" class="form-control">
                            @foreach($countries as $country)
                                <option>{{$country}}</option>
                            @endforeach
                        </select>

                        <label>@lang('adminlte::adminlte.keywords')</label>
                        <input type="text" name="keywords" class="form-control" value="{{old('keywords')}}">

                        <label>@lang('adminlte::adminlte.file')</label>
                        <input value="put" type="file" name="file" class="form-control">

                        <div class="radio ">
                            <label>
                                <input type="radio" name="option" value="0" checked>
                                <i class="fa fa-fw  fa-save"></i>
                                @lang('adminlte::adminlte.save_as_draft')
                            </label>
                        </div>
                        <div class="radio ">
                            <label>
                                <input type="radio" name="option" value="1">
                                <i class="fa fa-fw  fa-send-o"></i>
                                @lang('adminlte::adminlte.send_to_analyze')
                            </label>
                        </div>

                        {{--<div class="row col-xs-12 col-md-6">--}}
                        <br>
                        <input type="submit" value="@lang('adminlte::adminlte.save')"
                               class="btn btn-success btn-block">


                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </form>
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>

@endsection

@push('js')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#disease").select2({
                allowClear: true,
                placeholder: 'Search for a disease'
            });
        })
    </script>
@endpush