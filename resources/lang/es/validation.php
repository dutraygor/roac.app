<?php
    return [
        /*
        |--------------------------------------------------------------------------
        | Validation Language Lines
        |--------------------------------------------------------------------------
        |
        | The following language lines contain the default error messages used by
        | the validator class. Some of these rules have multiple versions such
        | as the size rules. Feel free to tweak each of these messages here.
        |
        */
        'accepted' => ':attribute debe ser aceptado.',
        'active_url' => ':attribute não é uma URL válida.',
        'after' => ':attribute deve ser uma data depois de :date.',
        'alpha' => ':attribute debe contener solamente letras.',
        'alpha_dash' => ':attribute debe contener letras, números y guiones.',
        'alpha_num' => ':attribute debe contener sólo letras y números.',
        'array' => ':attribute deve ser um array.',
        'before' => ':attribute deve ser uma data antes de :date.',
        'between' => [
            'numeric' => ':attribute deve estar entre :min e :max.',
            'file' => ':attribute deve estar entre :min e :max kilobytes.',
            'string' => ':attribute deve estar entre :min e :max caracteres.',
            'array' => ':attribute deve ter entre :min e :max itens.',
        ],
        'boolean' => ':attribute deve ser verdadeiro ou falso.',
        'confirmed' => 'La confirmación de :attribute no confiere.',
        'date' => ':attribute no es una fecha válida.',
        'date_format' => ':attribute não confere com o formato dd/mm/yyyy.',
        'different' => ':attribute e :other devem ser diferentes.',
        'digits' => ':attribute deve ter :digits dígitos.',
        'digits_between' => ':attribute deve ter entre :min e :max dígitos.',
        'email' => ':attribute deve ser um endereço de e-mail válido.',
        'exists' => 'El :attribute seleccionado no es válido.',
        'filled' => ':attribute es un campo obligatorio.',
        'image' => ':attribute debe ser una imagen.',
        'in' => ':attribute es inválido.',
        'integer' => ':attribute debe ser un entero.',
        'ip' => ':attribute deve ser um endereço IP válido.',
        'json' => ':attribute deve ser um JSON válido.',
        'max' => [
            'numeric' => ':attribute não deve ser maior que :max.',
            'file' => ':attribute no debe tener más que :max kilobytes.',
            'string' => ':attribute no debe tener más que :max caracteres.',
            'array' => ':attribute no puede tener más que :max itens.',
        ],
        'mimes' => ':attribute debe ser un archivo de tipo: :values.',
        'min' => [
            'numeric' => ':attribute deve ser no mínimo :min.',
            'file' => ':attribute debe tener como mínimo :min kilobytes.',
            'string' => ':attribute deve tener como mínimo :min caracteres.',
            'array' => ':attribute deve tener como mínimo :min itens.',
        ],
        'not_in' => 'El :attribute seleccionado no es válido.',
        'numeric' => ':attribute debe ser un número.',
        'regex' => 'El formato de :attribute es inválido.',
        'required' => 'El campo :attribute es obligatorio.',
        'required_if' => 'El campo :attribute es obligatorio cuando :other es :value.',
        'required_with' => 'El campo :attribute es obligatorio cuando :values está presente.',
        'required_with_all' => 'El campo :attribute es obligatorio cuando :values estão presentes.',
        'required_without' => 'El campo :attribute es obligatorio cuando :values não está presente.',
        'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de éstos está presente: :values.',
        'same' => ':attribute e :other deben ser iguales.',
        'size' => [
            'numeric' => ':attribute debe ser :size.',
            'file' => ':attribute debe tener :size kilobytes.',
            'string' => ':attribute debe tener :size caracteres.',
            'array' => ':attribute debe contener :size itens.',
        ],
        'string' => ':attribute debe ser una string',
        'timezone' => ':attribute debe ser una timezone válida.',
        'unique' => ':attribute ya está en uso.',
        'url' => 'El formato de :attribute es inválido.',
        'not_closed' => "Falla al cerrar la clase",


        /*
        |--------------------------------------------------------------------------
        | Custom Validation Language Lines
        |--------------------------------------------------------------------------
        |
        | Here you may specify custom validation messages for attributes using the
        | convention "attribute.rule" to name the lines. This makes it quick to
        | specify a specific custom language line for a given attribute rule.
        |
        */
        'custom' => [

            'questions.*.title' => [

                'required' => 'Algumas perguntas não foram preenchidas',
                'required_if' => 'Algumas perguntas não foram preenchidas',

            ],

            'questions.*.options.*.text' => [

                'required' => 'Algumas perguntas estão com opções não preenchidas',
                'required_if' => 'Algumas perguntas estão com opções não preenchidas',

            ],

            'questions.*.timestamp' => [

                'required' => 'Algumas perguntas sem um tempo especificado',
                'required_if' => 'Algumas perguntas sem um tempo especificado',
                'date_format' => 'Algumas perguntas estão com um tempo inválido',

            ],

            'questions.*.options' => [

                'required' => 'Algumas perguntas estão sem opções',
                'required_if' => 'Algumas perguntas estão sem opções',

            ],

            'contents.*.start_time' => [

                'required' => 'Alguns conteúdos estão sem um tempo inicial',
                'required_if' => 'Alguns conteúdos estão sem um tempo inicial',
                'date_format' => 'Alguns conteúdos estão com um tempo inicial inválido',

            ],

            'contents.*.end_time' => [

                'required' => 'Alguns conteúdos estão sem um tempo final',
                'required_if' => 'Alguns conteúdos estão sem um tempo final',
                'date_format' => 'Alguns conteúdos estão com um tempo final inválido',

            ],

            'contents.*.tags' => [

                'required' => 'Todos os conteúdos devem ter ao menos uma tag',
                'required_if' => 'Todos os conteúdos devem ter ao menos uma tag',

            ],

            'video_type' => [

                'required_if' => 'O campo tipo de vídeo é obrigatório',

            ],

            'video_id' => [

                'required_if' => 'O campo ID do vídeo no Youtube é obrigatório',

            ],

            'video_file' => [

                'required_if' => 'O campo Enviar arquivo é obrigatório',

            ],

            'handicap_type' => [

                'required_if' => 'Escolha qual o tipo de necessidade especial você possui',

            ],

            'handicap_custom_type' => [

                'required_if' => 'Especifique qual o tipo de necessidade especial você possui',

            ],

            'which_instrument' => [

                'required_if' => 'Diga qual instrumento musical você toca',

            ],

        ],
        /*
        |--------------------------------------------------------------------------
        | Custom Validation Attributes
        |--------------------------------------------------------------------------
        |
        | The following language lines are used to swap attribute place-holders
        | with something more reader friendly such as E-Mail Address instead
        | of "email". This simply helps us make messages a little cleaner.
        |
        */
        'attributes' => [

            'name' => trans('strings.name'),
            'email' => trans('auth.email_address'),
            'password' => trans('auth.password'),
            'knowledge_area' => trans('strings.knowledge_area'),
//            'title' => trans('strings.title'),
//            'description' => trans('strings.description'),
            'outro' => trans('strings.outro'),
            'video_type' => trans('strings.video_type'),
            'video_id' => trans('strings.youtube_video_id'),
            'video_file' => trans('strings.send_file'),
            'questions' => trans('strings.questions'),
            'contents' => trans('strings.contents'),
            'age' => trans('strings.age'),
            'gender' => trans('strings.gender'),
            'college' => 'Escola/Universidade',
//            'course' => trans('strings.course'),
            'handicap' => trans('strings.handicap'),
            'handicap_type' => trans('strings.handicap_type'),
            'handicap_custom_type' => trans('strings.handicap_custom_type'),
            'play_musical_instrument' => trans('strings.play_musical_instrument'),
            'which_instrument' => trans('strings.which_instrument'),
            'educational_level' => 'Grau de escolaridade',
            'race_ethnicity' => 'Etnia',
            'grade_average' => 'Média escolar',
            'social_status' => 'Status pessoal',
            'income_class' => 'Classe social',


            'title' => 'Título',
            'author' => 'Autores',
            'description' => 'Descripción',
            'goal' => 'Meta',
            'note' => 'Anotaciones',
            'theme' => 'Tema',
            'modality' => 'Modalidad',
            'content' => 'Contenido',
            'keywords' => 'Palabras clave',
            'file' => 'Expediente',
            'educ_level' => 'Grado de escolaridad',
            'course' => 'Curso',
            'subject' => 'Disciplina',
            'disability' => 'Discapacidad',
            'res_type' => 'Tipo do Recurso',

        ],

        'at_least_one_right' => 'Todas as perguntas precisam ter ao menos uma opção marcada como certa',

    ];