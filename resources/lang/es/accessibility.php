﻿<?php

return [


    'go_to_menu' => 'Ir al menú',
    'go_to_content' => 'Ir al contenido',
    'go_to_footer' => 'Ir al rodapié',
    'go_to_map' => 'Ir al Mapa',
    'high_contrast' => 'Alto Contraste',

];
