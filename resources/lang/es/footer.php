﻿<?php

return [


    'full_name' => 'Repositorio de objetos Acacia - ',
    'version' => 'Versión ',
    'contact' => 'Contacto',
    'map' => 'Mapa del sitio',
    'home' => 'Casa',
    'accessibility' => 'Accesibilidad'

];
