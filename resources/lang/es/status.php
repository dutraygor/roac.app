<?php

return [

    'user' => [

        'created' => 'Un e-mail fue enviado al usuario avisando sobre su registro',
        'updated' => 'Usuario actualizado con éxito',
        'blocked' => 'Se ha bloqueado su acceso',
        'none' => 'Ningún usuario encontrado',

    ],

    'object' => [

        'created' => 'El objeto se guardó como borrador',
        'submited' => 'El objeto se envió para el análisis',
        'updated' => 'El objeto se ha actualizado con éxito!',
        'blocked' => 'Se ha bloqueado su acceso',
        'none' => 'Ningún objeto encontrado',

    ],


];