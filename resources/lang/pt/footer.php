﻿<?php

return [


    'full_name' => 'Repositório de Objetos Acacia - ',
    'version' => 'Versão ',
    'contact' => 'Contato',
    'map' => 'Mapa do Site',
    'home' => 'Home',
    'accessibility' => 'Acessibilidade'

];
