<?php

return [

    'user' => [

        'created' => 'Um e-mail foi enviado para o usuário avisando sobre seu cadastro',
        'updated' => 'Usuário atualizado com sucesso',
        'blocked' => 'O seu acesso foi bloqueado',
        'none' => 'Nenhum usuário encontrado',

    ],

    'object' => [

        'created' => 'O objeto foi salvo como rascunho',
        'submited' => 'O objeto foi enviado para análise',
        'updated' => 'O objeto foi atualizado com sucesso!',
        'blocked' => 'O seu acesso foi bloqueado',
        'none' => 'Nenhum objeto encontrado',

    ],

];