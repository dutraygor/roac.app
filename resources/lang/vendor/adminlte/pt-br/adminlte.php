<?php

return [

    'add'                         => 'Adicionar',
    'email'                       => 'Email',
    'full_name'                   => 'Nome Completo',
    'i_forgot_my_password'        => 'Esqueci minha senha',
    'i_already_have_a_membership' => 'Eu já possuo uma conta',
    'login'                       => 'Login',
    'log_out'                     => 'Logout',
    'login_message'               => 'Faça login para iniciar',
    'name'                        => 'Nome',
    'password_reset_message'      => 'Senha Restaurada',
    'password'                    => 'Senha',
    'reset_password'              => 'Restaurar Senha',
    'registration'                => 'Cadastro',
    'register_message'            => 'Nova conta cadastrada',
    'retype_password'             => 'Redigite sua senha',
    'remember_me'                 => 'Lembrar-me',
    'register'                    => 'Cadastrar',
    'register_a_new_membership'   => 'Cadastrar uma nova conta',
    'send_password_reset_link'    => 'Enviar o link para restaurar senha',
    'sign_in'                     => 'Entrar',
    'toggle_navigation'           => 'Alternar navegação',
    'user'                        => 'Usuário',
    'birth'                        => 'Data de Nascimento',
    'gender'                       => 'Gênero',
    'role'                         => 'Role',
    'create'                       => 'Criar',
    'male'                         => 'Masculino',
    'female'                       => 'Feminino',
    'admin'                        => 'admin',
    'collaborator'                 => 'colaborador',
    'title'                        => 'Título',
    'author'                       => 'Autores',
    'description'                  => 'Descrição',
    'goal'                         => 'Objetivo',
    'notes'                        => 'Anotações',
    'theme'                        => 'Tema',
    'language'                     => 'Idioma',
    'country'                      => 'País',
    'keywords'                     => 'Palavras-chave',
    'modality'                     => 'Modalidade',
    'educ_level'                     => 'Grau de Escolaridade',
    'course'                     => 'Curso',
    'subject'                     => 'Disciplina',
    'content'                     => 'Conteúdo',
    'disability'                     => 'Incapacidade',
    'resource_type'                     => 'Tipo do Recurso',
    'learn_object'                     => 'Objeto de Aprendizagem',
    'advanced'                     => 'Busca Avançada',
    'file'                     => 'Arquivo',
    'registered_object' => 'Objeto registrado: ',
    'insert_object' => 'Insertar objeto',
    'details_of_object'=>'Detalles del objeto',
    'see' => 'Visualizar',
    'way' => 'Camino',
    'search' => 'Pesquisa',


    'save_changes_finalize_later' => 'Guardar cambios y finalizar después',
    'end_send_review' => 'Finalizar y Enviar a Análisis',
    'save_as_draft' => 'Guardar como Borrador',
    'send_to_analyze' => 'Enviar a Análisis',
    'save' => 'Guardar',
    'cancel' => 'Cancelar',
    'back' => 'Regreso',



    'object' => [

        'created' => 'Objeto criado com sucesso!',
        'updated' => 'Objeto atualizado com sucesso!',
        'deleted' => 'Objeto excluído com sucesso!',
        'none' => 'Nenhum Objeto encontrado',
        'approved' => 'Objeto aprovado',


        'opt_approved' => 'Aprovado',
        'opt_rej_adj' => 'Rejeitados para ajustes',
        'opt_rej_ever' => 'Rejeitados',
        'opt_waiting_rev' => 'Aguardando análise',
        'opt_draft' => 'Rascunhos',

    ],




    'choose_a_content' => 'Selecione um conteúdo',
    'choose_a_course' => 'Selecione um curso',
    'choose_a_disability' => 'Selecione uma deficiência',
    'choose_a_educational_level' => 'Selecione um nível de escolaridade',
    'choose_a_modality' => 'Selecione uma modalidade',
    'choose_a_resource_type' => 'Selecione o tipo de recurso',
    'choose_a_subject' => 'Selecione um assunto',


    'my_objects' => 'Meus objetos',
    'all_objects' =>'Todos os objetos',
    'created_by' => 'Cadastrado por',
    'object_analysis' => 'Análise de Objeto',
    'published_objects' => 'Objetos publicados',


    'status' => 'Status',
    'created' => 'Cadastrado em',
    'actions' => 'Ações',


    'format' => 'Formato',
    'size' => 'Tamanho',
    'approved_in' => 'Aprovado em',
    'rejected_in' => 'Rejeitado em',
    'latest_review' => 'Última revisão',
    'motivation' => 'Motivação',
    'orientation' => 'Orientação',



    'view' => 'Visualizar',
    'view_pendency' => 'Visualizar pendências',
    'edit' => 'Editar',
    'delete' => 'Excluir',
    'delete_ever' => 'Excluir permanentemente',
    'analyze' => 'Analizar',
    'turn_to_analysis' => 'Voltar para análise',
    'rej_to_analysis' => 'Rejeitar para ajustes',
    'rej_def' => 'Rejeitar definitivamente',
    'approve' => 'Aprovar',




];
