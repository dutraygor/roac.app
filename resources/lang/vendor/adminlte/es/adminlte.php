<?php

return [

    'full_name' => 'Nombre completo',
    'email' => 'Email',
    'password' => 'Contraseña',
    'retype_password' => 'Vuelva la contraseña',
    'remember_me' => 'Recuérdame',
    'register' => 'Registre',
    'register_a_new_membership' => 'Registre una nueva cuenta',
    'i_forgot_my_password' => 'Olvidé mi contraseña',
    'i_already_have_a_membership' => 'Ya tengo una cuenta',
    'sign_in' => 'Registrarse',
    'log_out' => 'Cerrar',
    'toggle_navigation' => 'Navegación de palanca',
    'login_message' => 'Autenticarse para iniciar la sesión',
    'register_message' => 'Registre una nueva cuenta',
    'password_reset_message' => 'Restablecer la contraseña',
    'reset_password' => 'Restablecer la contraseña',
    'send_password_reset_link' => 'Enviar link de restablecimiento de contraseña',
    'login' => 'Iniciar Sesión',
    'add' => 'Añadir',
    'name' => 'Nombre',
    'registration' => 'Registro',
    'user' => 'Usuario',
    'birth' => 'Fecha de nacimiento',
    'gender' => 'Género',
    'role' => 'Papel',
    'create' => 'Crear',
    'male' => 'Masculino',
    'female' => 'Feminino',
    'admin' => 'admin',
    'collaborator' => 'contribuyente',
    'title' => 'Título',
    'author' => 'Autor(es)',
    'description' => 'Descripción',
    'goal' => 'Objetivo',
    'notes' => 'Anotaciones',
    'theme' => 'Tema',
    'language' => 'Idioma',
    'country' => 'País',
    'keywords' => 'Palabras clave',
    'modality' => 'Modalidad',
    'educ_level' => 'Nivel de escolaridad',
    'course' => 'Área de conocimiento',
    'subject' => 'Disciplina',
    'content' => 'Contenido',
    'disability' => 'Discapacidad',
    'resource_type' => 'Tipo do Recurso',
    'learn_object' => 'Objeto de Aprendizaje',
    'advanced' => 'Búsqueda Avanzada',
    'file' => 'Expediente',
    'registered_object' => 'Objeto registrado: ',
    'insert_object' => 'Insertar objeto',
    'details_of_object'=>'Detalles del objeto',
    'see' => 'Visualizar',
    'way' => 'Camino',
    'search' => 'Busca',


    'save_changes_finalize_later' => 'Guardar cambios y finalizar después',
    'end_send_review' => 'Finalizar y Enviar a Análisis',
    'save_as_draft' => 'Guardar como Borrador',
    'send_to_analyze' => 'Enviar a Análisis',
    'save' => 'Guardar',
    'cancel' => 'Cancelar',
    'back' => 'Regreso',

    'object' => [

        'created' => '¡Objeto creado con éxito!',
        'updated' => '¡Objeto actualizado con éxito!',
        'deleted' => 'Objeto eliminado con éxito!',
        'none' => 'Ningún objeto encontrado',
        'approved' => 'Objeto aprobado',


        'opt_approved' => 'Aprobado',
        'opt_rej_adj' => 'Rechazados para ajustes',
        'opt_rej_ever' => 'Rechazados',
        'opt_waiting_rev' => 'Aguardando el análisis',
        'opt_draft' => 'Borrar',

    ],



    'choose_a_content' => 'Seleccione un contenido',
    'choose_a_course' => 'Seleccione un curso',
    'choose_a_disability' => 'Seleccione una discapacidad',
    'choose_a_educational_level' => 'Seleccione un nivel de educación',
    'choose_a_modality' => 'Seleccione una modalidad',
    'choose_a_resource_type' => 'Seleccione el tipo de recurso',
    'choose_a_subject' => 'Seleccione un asunto',


    'my_objects' => 'Mis objetos',
    'all_objects' => 'Todos los objetos',
    'object_analysis' => 'Análisis de objetos',
    'published_objects' => 'Objetos publicados',

    'created_by' => 'Registrado por',
    'format' => 'Formato',
    'size' => 'Tamaño',
    'approved_in' => 'Aprobado en',
    'rejected_in' => 'Rechazado en',
    'latest_review' => 'Ultima revisión',
    'motivation' => 'Motivación',
    'orientation' => 'Dirección',
    'status' => 'Status',
    'created' => 'Registro en',
    'actions' => 'Acciones',


    'view' => 'Visualizar Objeto',
    'view_pendency' => 'Visualizar Pendencia',
    'edit' => 'Editar',
    'delete' => 'Borrar',
    'delete_ever' => 'Borrar permanentemente',
    'analyze' => 'Analizar',
    'turn_to_analysis' => 'Volver al análisis',
    'rej_to_analysis' => 'Rechazar para ajustes',
    'rej_def' => 'Rechazar definitivamente',
    'approve' => 'Aprobar',

    'advanced_search' => 'Búsqueda Avanzada',




];
