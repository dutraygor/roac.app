<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adjustment extends Model
{
    protected $table = 'adjustments';


    public static function createNew($data)
    {

        $adjustment = new Adjustment();

        $adjustment->description = $data['adjustment'];
        $adjustment->save();

    }

    public static function max_id()
    {

        $max_id = Adjustment::all('id')->max('id');

        return $max_id;
    }



}