<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lobject_Subject extends Model
{

    protected $table = 'lobject_subject';


    public function lObjects(){
        return $this->belongsToMany(Lobject::class);
    }

    public static function deleteObjects($object_id){
        $losub = Lobject_Subject::all()->where('lobject_id','=', $object_id);
        foreach ($losub as $lo){
            $x = $lo->delete();
        }
        return true;
    }

}
