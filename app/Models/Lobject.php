<?php

namespace App\Models;

use App\Http\Requests\UpdateLobjectRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Lobject extends Model
{
    protected $table = 'lobjects';


    public function store($request, $file, $path)
    {
        $this->title = $request['title'];
        $this->author = $request['author'];
        $this->description = $request['description'];
        $this->goal = $request['goal'];
        $this->notes = $request['note'];
        $this->theme = $request['theme'];
        $this->modality_id = $request['modality'];
        $this->resource_type_id = $request['res_type'];
        $this->language = $request['lang'];
        $this->country = $request['country'];
        $this->key_words = $request['keywords'];

        $this->format = $file['file']['type'];
        $this->size = $file['file']['size'];
        $this->path = $path;
        $this->rights = 'Direitos teste';
        $this->license = 'license teste';
        $this->user_id = Auth::user()->id;

        $this->save();

        $this->educationalLevels()->attach($request['educ_level'], ['lobject_id' => $this->id]);
        $this->courses()->attach($request['course'], ['lobject_id' => $this->id]);
        $this->subjects()->attach($request['subject'], ['lobject_id' => $this->id]);
        $this->contents()->attach($request['content'], ['lobject_id' => $this->id]);
        $this->disabilities()->attach($request['disability'], ['lobject_id' => $this->id]);

        Lobject_State::createNew($this->id, $request['option']);
        $los_id = Lobject_State::max_id();
        $this->updateLosId($los_id);

    }

    public function updateData($data)
    {


        $this->title = $data['title'];
        $this->author = $data['author'];
        $this->description = $data['description'];
        $this->goal = $data['goal'];
        $this->notes = $data['note'];
        $this->theme = $data['theme'];
        $this->modality_id = $data['modality'];
        $this->resource_type_id = $data['res_type'];
        $this->language = $data['lang'];
        $this->country = $data['country'];
        $this->key_words = $data['keywords'];

        if ($data['file'] != null) {

            $file = $_FILES;
            $directory = 'repo/';
            $path = $directory . $_FILES['file']['name'];
            move_uploaded_file($_FILES['file']['tmp_name'], $path);

            $this->format = $file['file']['type'];
            $this->size = $file['file']['size'];
            $this->path = $path;

        }

        $this->rights = 'Missing rights';
        $this->license = 'Missing License';
        //$this->user_id = Auth::user()->id;


        if($data['option'] == '1'){
            Lobject_State::createNew($this->id, $data['option']);
            $los_id = Lobject_State::max_id();
            $this->updateLosId($los_id);
        }


        $this->save();

        EducLevel_Lobject::deleteObjects($this->id);
        $this->educationalLevels()->attach($data['educ_level'], ['lobject_id' => $this->id]);
        Course_Lobject::deleteObjects($this->id);
        $this->courses()->attach($data['course'], ['lobject_id' => $this->id]);
        Lobject_Subject::deleteObjects($this->id);
        $this->subjects()->attach($data['subject'], ['lobject_id' => $this->id]);
        Content_Lobject::deleteObjects($this->id);
        $this->contents()->attach($data['content'], ['lobject_id' => $this->id]);
        Disability_Lobject::deleteObjects($this->id);
        $this->disabilities()->attach($data['disability'], ['lobject_id' => $this->id]);


    }


    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function los()
    {
        return $this->belongsTo(Lobject_State::class, 'los_id');

    }

    public function adjustments()
    {

        return $this->belongsToMany(Adjustment::class);

    }

    public static function getObject($id)
    {
        $object = Lobject::findOrFail($id);
        return $object;
    }

    public static function getCreator($id_user)
    {
        $creator = User::findOrFail($id_user);
        return $creator;
    }


    public function updateLosId($los_id)
    {
        $this->los_id = $los_id;
        return $this->save();
    }

    public function updateReasonOfRejection($data)
    {
        $this->reason_of_rejection = $data['reason'];
        return $this->save();
    }

    public function educationalLevels()
    {
        return $this->belongsToMany(EducationalLevel::class, 'educ_level_lobject')
            ->as('relate_educ');
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'course_lobject');
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'lobject_subject');
    }

    public function contents()
    {
        return $this->belongsToMany(Content::class, 'content_lobject');
    }

    public function disabilities()
    {
        return $this->belongsToMany(Disability::class, 'disability_lobject');
    }

    public function resourceType(){
        return $this->belongsTo(ResourceType::class);
    }
}
