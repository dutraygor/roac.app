<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content_Lobject extends Model
{

    protected $table = 'content_lobject';


    public function lObjects(){
        return $this->belongsToMany(Lobject::class);
    }

    public static function deleteObjects($object_id){
        $conLob = Content_Lobject::all()->where('lobject_id','=', $object_id);
        foreach ($conLob as $lo){
            $x = $lo->delete();
        }
        return true;
    }

}
