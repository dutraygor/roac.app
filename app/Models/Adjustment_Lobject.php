<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adjustment_Lobject extends Model
{
    protected $table = 'adjustment_lobject';


    public static function getAdjustments($object_id)
    {

        $max_id = Adjustment_Lobject::all()
            ->where('lobject_id','=',$object_id)
            ->max('id');

        $adj_id = Adjustment_Lobject::all()
            ->where('id','=',$max_id)
            ->max('adjustment_id');

        $description = Adjustment::findOrFail($adj_id);

        return $description;
    }


    public static function createNew($data, $id_adj)
    {

        $adj_obj = new Adjustment_Lobject();

        $adj_obj->lobject_id = $data['object_id'];
        $adj_obj->adjustment_id = $id_adj;
        $adj_obj->save();

    }

    public static function deleteObjects($object_id){
        $adjls = Adjustment_Lobject::all()->where('lobject_id','=', $object_id);
        foreach ($adjls as $lo){
            $x = $lo->delete();
        }
        return true;
    }



}