<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducLevel_Lobject extends Model
{

    protected $table = 'educ_level_lobject';


    public function lObjects(){
        return $this->belongsToMany(Lobject::class);
    }


    public static function deleteObjects($object_id){
        $edulobj = EducLevel_Lobject::all()->where('lobject_id','=', $object_id);
        foreach ($edulobj as $lo){
            $x = $lo->delete();
        }
        return true;
    }


}
