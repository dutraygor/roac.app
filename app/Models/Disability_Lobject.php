<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disability_Lobject extends Model
{

    protected $table = 'disability_lobject';


    public function lObjects(){
        return $this->belongsToMany(Lobject::class);
    }


    public static function deleteObjects($object_id){
        $dislob = Disability_Lobject::all()->where('lobject_id','=', $object_id);
        foreach ($dislob as $lo){
            $x = $lo->delete();
        }
        return true;
    }

}
