<?php

namespace App\Models;

use App\Http\Requests\UserRegistrationRequest;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
class User extends Authenticatable
{
    use Notifiable;

    public function votes(){
        return $this->hasMany(Vote::class);
    }

    /**
     * User's roles.
     */
    const ADMIN = 'admin';
    const COLLABORATOR = 'collaborator';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Checks if the user is active or not.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active == true;
    }

    /**
     * Checks if the user is an admin.
     *
     * @return bool
     */
    public function isAdmin()
    {

        return $this->role == static::ADMIN;

    }

    /**
     * Checks if the user is a collaborator.
     *
     * @return bool
     */
    public function isCollaborator()
    {

        return $this->role == static::COLLABORATOR;

    }

    public function store($request){
        $this->name = $request['name'];
        $this->email = $request['email'];
        $this->password = bcrypt($request['password']);
        $this->role = $request['role'];
        $this->active = true;
        $this->save();

        return $this;
    }
}

