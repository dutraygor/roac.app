<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;


class Lobject_State extends Model
{
    protected $table = 'lobject_state';

    public static function getLastStateByObject($object_id)
    {

        $ultimate = DB::table('lobject_state')
            ->select('order')
            ->where('lobject_id', $object_id)
            ->orderBy('order', 'desc')
            ->value('order');

        return $ultimate;
    }


    public static function getL_O_S($id)
    {
        $los = Lobject_State::findOrFail($id);
        return $los;
    }


    public static function createNew($object_id, $state_id)
    {
        $data = now();

        $los = new Lobject_State();
        $los->lobject_id = $object_id;
        $los->state_id = $state_id;
        $los->order = $data;
        if ($los->save()) {
            return true;
        } else {
            return false;
        }
    }


    public static function max_id()
    {

        $max_id = Lobject_State::all('id')->max('id');

        return $max_id;
    }



    public static function deleteObjects($object_id){
        $los = Lobject_State::all()->where('lobject_id','=', $object_id);
        foreach ($los as $lo){
            $x = $lo->delete();
        }
        return true;
    }


}
