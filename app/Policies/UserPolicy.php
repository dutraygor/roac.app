<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Only admins can see menus.
     *
     * @param User $user
     * @param string $ability
     * @return bool
     */
    public function before(User $user, string $ability)
    {

    }

}
