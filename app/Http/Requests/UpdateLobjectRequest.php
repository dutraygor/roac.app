<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateLobjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        if (Auth::check()) {
            return true;
        } else
            return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'goal' => 'required',
            'note' => 'required',
            'theme' => 'required',
            'modality' => 'required',
            'res_type' => 'required',
            'lang' => 'required',
            'country' => 'required',
            'keywords' => 'required',
            'educ_level' => 'required',
            'course' => 'required',
            'subject' => 'required',
            'content' => 'required',
            'disability' => 'required',

        ];
    }
}
