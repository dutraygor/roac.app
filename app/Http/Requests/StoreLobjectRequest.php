<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreLobjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'author' => 'required|max:80',
            'description' => 'required|max:400',
            'goal' => 'required|max:400',
            'note' => 'required|max:400',
            'theme' => 'required|max:255',
            'modality' => 'required',
            'educ_level' => 'required',
            'course' => 'required',
            'subject' => 'required',
            'content' => 'required',
            'disability' => 'required',
            'res_type' => 'required',
            'lang' => 'required',
            'country' => 'required',
            'keywords' => 'required|max:30',
            'file' => 'required'

        ];
    }
}
