<?php

namespace App\Http\Controllers;


use App\Http\Requests\LobjectRegistrationRequest;
use App\Http\Requests\StoreLobjectRequest;
use App\Http\Requests\UpdateLobjectRequest;
use App\Http\Requests\UpdateRejAdjObjRequest;
use App\Http\Requests\UpdateRejDefObjRequest;
use App\Models\Adjustment;
use App\Models\Content;
use App\Models\Content_Lobject;
use App\Models\Course;
use App\Models\Course_Lobject;
use App\Models\Disability;
use App\Models\Disability_Lobject;
use App\Models\EducationalLevel;
use App\Models\EducLevel_Lobject;
use App\Models\Lobject;
use App\Models\Lobject_State;
use App\Models\Lobject_Subject;
use App\Models\Lobject_User_Vote;
use App\Models\Modality;
use App\Models\ResourceType;
use App\Models\Adjustment_Lobject;
use App\Models\Subject;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;


class LobjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::check()) {

            if (Auth::user()->isAdmin()) {
                //all objects
                $objects = $this->getAllObjects();
                return view('registers.object.index_admin', compact('objects'));

            } elseif (Auth::user()->isCollaborator()) {
                $objects = $this->getObjectsOfUser();
                return view('registers.object.index', compact('objects'));
            }
        }

    }


    private function getObjectsOfUser()
    {
        $user = Auth::user();
        return $objects = Lobject::with('los')
            ->join('lobject_state', 'lobjects.id', 'lobject_state.lobject_id')
            ->join('states', 'lobject_state.state_id', 'states.id')
            ->whereColumn('lobjects.los_id', 'lobject_state.id')
            ->where([
                ['user_id', '=', $user->id],
                ['states.id', '<>', 5]
            ])
            ->orderBy('lobject_state.order', 'asc')
            ->paginate(10);
    }


    private function getAllObjects()
    {
        return $objects = Lobject::with('los', 'creator')
            ->join('lobject_state', 'lobjects.id', 'lobject_state.lobject_id')
            ->join('states', 'lobject_state.state_id', 'states.id')
            ->whereColumn('lobjects.los_id', 'lobject_state.id')
            ->where('states.id', '<>', 5)
            ->orderBy('lobject_state.order', 'asc')
            ->paginate(10);
    }


    //========================================== List admin ==========================================================


    public function listObjects($state_id)
    {

        if (Auth::check()) {

            $user = Auth::user();

            if (Auth::user()->isAdmin()) {

                $objects = Lobject::with('creator', 'los')
                    ->join('lobject_state', 'lobjects.id', 'lobject_state.lobject_id')
                    ->where('lobject_state.state_id', $state_id)
                    ->whereColumn('lobjects.los_id', 'lobject_state.id')
                    ->orderBy('lobject_state.order', 'asc')
                    ->paginate('10');

                if ($state_id == 1) {
                    return view('registers.object.admin.awaiting_review', compact('objects'));
                } elseif ($state_id == 2) {
                    return view('registers.object.admin.approved', compact('objects'));
                } elseif ($state_id == 3) {
                    return view('registers.object.admin.rejected_with_pendency', compact('objects'));
                } elseif ($state_id == 4) {
                    return view('registers.object.admin.rejected', compact('objects'));
                } elseif ($state_id == 0) {
                    return view('registers.object.admin.draft', compact('objects','user'));
                } else {
                    $objects = $this->getAllObjects();
                    return view('registers.object.index_admin', compact('objects'));
                }

            } else {


                $objects = Lobject::with('creator', 'los')
                    ->join('lobject_state', 'lobjects.id', 'lobject_state.lobject_id')
                    ->where('lobject_state.state_id', $state_id)
                    ->where('lobjects.user_id', '=', $user->id)
                    ->whereColumn('lobjects.los_id', 'lobject_state.id')
                    ->orderBy('lobject_state.order', 'asc')
                    ->paginate('10');
            }

            if ($state_id == 1) {
                return view('registers.object.collaborator.awaiting_review', compact('objects'));
            } elseif ($state_id == 2) {
                return view('registers.object.collaborator.approved', compact('objects'));
            } elseif ($state_id == 3) {
                return view('registers.object.collaborator.rejected_with_pendency', compact('objects'));
            } elseif ($state_id == 4) {
                return view('registers.object.collaborator.rejected', compact('objects'));
            } elseif ($state_id == 0) {
                return view('registers.object.collaborator.draft', compact('objects'));
            } else {
                $objects = $this->getObjectsOfUser();
                return view('registers.object.index', compact('objects'));
            }

        }
    }


    //========================================== End List admin =======================================================


    // ================================ open the pendent object for analyze ==========================================

    public function analyze_object($object_id)
    {

        $object = Lobject::getObject($object_id);
        //echo $object;

        $creator = Lobject::getCreator($object->user_id);

        $modality = Modality::findOrFail($object->modality_id);

        $resource_type = ResourceType::findOrFail($object->resource_type_id);

        $los = Lobject_State::getL_O_S($object->los_id);

        $language = $object->language;


        $elo = EducLevel_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('educational_level_id');
        $educ_level = EducationalLevel::all()->whereIn('id', $elo);

        $clo = Course_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('course_id');
        $courses = Course::all()->whereIn('id', $clo);

        $losub = Lobject_Subject::all()->where('lobject_id', '=', $object->id)->pluck('subject_id');
        $subjects = Subject::all()->whereIn('id', $losub);

        $contlob = Content_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('content_id');
        $contents = Content::all()->whereIn('id', $contlob);

        $disablob = Disability_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('disability_id');
        $disabilities = Disability::all()->whereIn('id', $disablob);


        return view('registers.object.options.admin.object_analysis',
            compact('object', 'creator', 'modality', 'language', 'los',
                'resource_type', 'educ_level', 'courses', 'subjects', 'contents', 'disabilities'));
    }

    // ==================================== End Analyze=============================================================


    // ========================================== Visualise Object Approved ========================================

    public function view_object($object_id)
    {

        $object = Lobject::getObject($object_id);
        //echo $object;

        $creator = Lobject::getCreator($object->user_id);

        $modality = Modality::findOrFail($object->modality_id);

        $resource_type = ResourceType::findOrFail($object->resource_type_id);

        $los = Lobject_State::getL_O_S($object->los_id);

        $language = $object->language;


        $elo = EducLevel_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('educational_level_id');
        $educ_level = EducationalLevel::all()->whereIn('id', $elo);

        $clo = Course_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('course_id');
        $courses = Course::all()->whereIn('id', $clo);

        $losub = Lobject_Subject::all()->where('lobject_id', '=', $object->id)->pluck('subject_id');
        $subjects = Subject::all()->whereIn('id', $losub);

        $contlob = Content_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('content_id');
        $contents = Content::all()->whereIn('id', $contlob);

        $disablob = Disability_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('disability_id');
        $disabilities = Disability::all()->whereIn('id', $disablob);


        return view('registers.object.options.both.view_object',
            compact('object', 'creator', 'modality', 'language', 'los',
                'resource_type', 'educ_level', 'courses', 'subjects', 'contents', 'disabilities'));
    }

    // =================================End Visualise ===============================================================


    // ================================ To Approve Object ===========================================================
    public function to_approve($object_id)
    {

        $object = Lobject::findOrFail($object_id);

        if (Lobject_State::createNew($object->id, 2)) {

            $los_id = Lobject_State::max_id();

            $object->updateLosId($los_id);

        }

        //retorna para listagem de Objetos Pendentes
        return $this->btnBack(1);

    }
    // =================================End To Approve ==============================================================


    // ================================ Delete Object Logically =====================================================
    public function delete_logically($object_id)
    {

        $object = Lobject::findOrFail($object_id);

        if (Lobject_State::createNew($object->id, 5)) {

            $los_id = Lobject_State::max_id();

            $object->updateLosId($los_id);

        }

        //retorna para listagem de Objetos Aprovados
        return $this->btnBack(2);

    }
    // =================================End Delete ==============================================================


    // ================================ Delete Object =====================================================
    public function delete_ever($object_id)
    {

        //exclui o campo los_id
        //exclui o Los
        //exclui o Lobject

        $object = Lobject::getObject($object_id);

        $los_id = $object->los_id;

        $vazio = null;
        $object->updateLosId($vazio);


        $l_ob_st = Lobject_State::getL_O_S($los_id);
        $state_id = $l_ob_st->state_id;


        Lobject_State::deleteObjects($object_id);
        Adjustment_Lobject::deleteObjects($object_id);
        EducLevel_Lobject::deleteObjects($object_id);
        Disability_Lobject::deleteObjects($object_id);
        Content_Lobject::deleteObjects($object_id);
        Course_Lobject::deleteObjects($object_id);
        Lobject_Subject::deleteObjects($object_id);
        Lobject_User_Vote::deleteObjects($object_id);


        $object->delete();

        //retorna para listagem
        return redirect()->route('list.objects', ['state_id' => $state_id]);

    }
    // =================================End Delete ==============================================================


    // ================================ Turn to analysis =====================================================
    public function turn_to_analysis($object_id)
    {

        $object = Lobject::findOrFail($object_id);

        if (Lobject_State::createNew($object->id, 1)) {

            $los_id = Lobject_State::max_id();

            $object->updateLosId($los_id);

            $data = ['reason' => null];

            $object->updateReasonOfRejection($data);

        }

        //retorna para listagem de Objetos Rejeitados
        return $this->btnBack(4);

    }
    // =================================End Turn ==============================================================


    /**
     * Store a newly created resource in storage.
     * @param  \App\Http\Requests\UpdateRejDefObjRequest $request
     * @return \Illuminate\Http\Response
     */
    public function reject_definitively(UpdateRejDefObjRequest $request)
    {

        $data = $request->only([
            'reason',
            'object_id',
        ]);

        $object = Lobject::findOrFail($data['object_id']);

        if (Lobject_State::createNew($object->id, 4)) {

            $los_id = Lobject_State::max_id();

            $object->updateLosId($los_id);

            $object->updateReasonOfRejection($data);

        }

        //retorna para listagem de Objetos aguardando analise
        return redirect()->route('list.objects', ['state_id' => 1]);

    }

    /**
     * Store a newly created resource in storage.
     * @param  \App\Http\Requests\UpdateRejAdjObjRequest $request
     * @return \Illuminate\Http\Response
     */
    public function reject_adjustment(UpdateRejAdjObjRequest $request)
    {

        $data = $request->only([
            'adjustment',
            'object_id',
        ]);

        Adjustment::createNew($data);

        $id_adj = Adjustment::max_id();


        Adjustment_Lobject::createNew($data, $id_adj);

        $object = Lobject::findOrFail($data['object_id']);

        if (Lobject_State::createNew($object->id, 3)) {

            $los_id = Lobject_State::max_id();

            $object->updateLosId($los_id);
        }


        //retorna para listagem de Objetos aguardando analise
        return redirect()->route('list.objects', ['state_id' => 1]);

    }


    //working yet
    public function view_pendency($object_id)
    {

        $pendency = Adjustment_Lobject::getAdjustments($object_id);

        $object = Lobject::getObject($object_id);

        return view('registers.object.options.collaborator.view_pendency_of_object', compact('pendency','object'));
        //return  compact('pendency');
    }


//Botao Voltar adaptavel nas paginas de opçoes
    public function btnBack($state_id)
    {
        return redirect(route('list.objects', ['state_id' => $state_id]));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langs = collect(trans('adminlte::langlist'));
        $countries = collect(trans('adminlte::countrylist'));
        return view('registers.object.create', compact('langs', 'countries'));
    }


    /** /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     * @param StoreLobjectRequest $request
     */
    public function store(StoreLobjectRequest $request)
    {
        $lobject = new Lobject();
//        $file = $request->file('file');
        $file = $_FILES;


        $directory = 'repo/';

        if (!file_exists($directory)) {
            mkdir('repo/');
        }


        $path = $directory . $_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $path);

        $lobject->store($request, $file, $path);

        return redirect()->route('dashboard.object.index')->with('status', trans('status.object.created'));
    }

    public function move()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function edit_object($object_id)
    {

        $object = Lobject::getObject($object_id);

        $modality_obj = Modality::findOrFail($object->modality_id);

        $resource_type = ResourceType::findOrFail($object->resource_type_id);

        $los = Lobject_State::getL_O_S($object->los_id);

        $state_id = $los->state_id;

        $elo = EducLevel_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('educational_level_id');
        $educ_level_in = EducationalLevel::all()->whereIn('id', $elo);
        $educ_level_out = EducationalLevel::all()->whereNotIn('id', $elo);

        $clo = Course_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('course_id');
        $course_in = Course::all()->whereIn('id', $clo);
        $course_out = Course::all()->whereNotIn('id', $clo);

        $losub = Lobject_Subject::all()->where('lobject_id', '=', $object->id)->pluck('subject_id');
        $subject_in = Subject::all()->whereIn('id', $losub);
        $subject_out = Subject::all()->whereNotIn('id', $losub);

        $contlob = Content_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('content_id');
        $content_in = Content::all()->whereIn('id', $contlob);
        $content_out = Content::all()->whereNotIn('id', $contlob);

        $disablob = Disability_Lobject::all()->where('lobject_id', '=', $object->id)->pluck('disability_id');
        $disability_in = Disability::all()->whereIn('id', $disablob);
        $disability_out = Disability::all()->whereNotIn('id', $disablob);


        $langs = collect(trans('adminlte::langlist'));
        $countries = collect(trans('adminlte::countrylist'));


        $old_object = $object->path;
        $old_object = substr_replace($old_object, '', '', '5');


        $user = Auth::user();


        if ($state_id == 2 ) {
            return view('registers.object.options.both.edit_when_approved',
                compact('object', 'langs', 'countries', 'modality_obj',
                    'resource_type', 'educ_level_in', 'educ_level_out', 'course_in', 'course_out',
                    'subject_in', 'subject_out', 'content_in', 'content_out', 'disability_in', 'disability_out', 'old_object'));
        } elseif ($state_id == 0 ) {
            if ($user->id == $object->user_id) {
                return view('registers.object.options.both.edit_when_draft',
                    compact('object', 'langs', 'countries', 'modality_obj',
                        'resource_type', 'educ_level_in', 'educ_level_out', 'course_in', 'course_out',
                        'subject_in', 'subject_out', 'content_in', 'content_out', 'disability_in', 'disability_out', 'old_object'));
            }else{
                return $this->listObjects(0);
            }
        } elseif ($state_id == 3 ) {
            return view('registers.object.options.both.edit_when_rej_adj',
                compact('object', 'langs', 'countries', 'modality_obj',
                    'resource_type', 'educ_level_in', 'educ_level_out', 'course_in', 'course_out',
                    'subject_in', 'subject_out', 'content_in', 'content_out', 'disability_in', 'disability_out', 'old_object'));
        }


    }


    public function update(UpdateLobjectRequest $request, $id)
    {
        $object = Lobject::getObject($id);

        $los_id = $object->los_id;

        $object->updateData($request);

        $l_ob_st = Lobject_State::getL_O_S($los_id);
        $state_id = $l_ob_st->state_id;

        //retorna para a listagem
        return redirect()->route('list.objects', ['state_id' => $state_id])->with('status', trans('status.object.updated'));

    }


    // ================================= Deletar objeto fisicamente =================================================

    public function destroy($id)
    {

        //exclui o campo los_id
        //exclui o Los
        //exclui o Lobject

        $object = Lobject::getObject($id);

        $los_id = $object->los_id;

        $vazio = null;
        $object->updateLosId($vazio);


        $l_ob_st = Lobject_State::getL_O_S($los_id);
        $state_id = $l_ob_st->state_id;
        $l_ob_st->delete();

        $object->delete();

        //retorna para listagem
        return redirect()->route('list.objects', ['state_id' => $state_id]);

    }
}
