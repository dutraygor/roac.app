<?php

namespace App\Http\Controllers;

use App\Models\Lobject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    public function advancedSearch(Request $request)
    {
        $search = $request['search'];
        $author = $request['author'];
        $educ_level = $request['educ_level'];
        $disability = $request['disability'];
        $theme = $request['theme'];
        $content = $request['content'];
        $res_type = $request['res_type'];

        if ($request['with_author'] || ($request['with_disability']) || $request['with_educ_level'] ||
            $request['with_theme'] ||
            $request['with_content'] ||
            $request['with_res_type']) {

            $results = Lobject::with('educationalLevels', 'contents', 'disabilities', 'resourceType')
//                    ->select()
                ->join('lobject_state', 'lobjects.id', '=', 'lobject_state.lobject_id')
                ->join('states', 'lobject_state.state_id', '=', 'states.id')
                ->join('educ_level_lobject', 'lobjects.id', '=', 'educ_level_lobject.lobject_id')
                ->join('educational_levels', 'educational_levels.id', '=', 'educ_level_lobject.educational_level_id')
                ->join('content_lobject', 'lobjects.id', '=', 'content_lobject.lobject_id')
                ->join('contents', 'contents.id', '=', 'content_lobject.content_id')
                ->join('disability_lobject', 'lobjects.id', '=', 'disability_lobject.lobject_id')
                ->join('disabilities', 'disabilities.id', '=', 'disability_lobject.disability_id')
                ->join('resource_types', 'resource_types.id', '=', 'lobjects.resource_type_id')
                ->where('states.id', '=', '2')
                ->where(DB::raw('lower(title)'), 'like', DB::raw("lower('%{$search}%')"))
                ->where(DB::raw('lower(author)'), 'like', DB::raw("lower('%{$author}%')"))
                ->where(DB::raw('lower(theme)'), 'like', DB::raw("lower('%{$theme}%')"))
                ->where(DB::raw('lower(educational_levels.level)'), 'like', DB::raw("lower('%{$educ_level}%')"))
                ->where(DB::raw('lower(contents.name)'), 'like', DB::raw("lower('%{$content}%')"))
                ->where(DB::raw('lower(disabilities.name)'), 'like', DB::raw("lower('%{$disability}%')"))
                ->where(DB::raw('lower(resource_types.name)'), 'like', DB::raw("lower('%{$res_type}%')"))
                ->orderBy('lobject_state.order', 'desc')
                ->paginate(20);


        } else {
            return $this->search($request);
        }
        return view('searches.basic', compact('results'));
    }

    public function search(Request $request)
    {

        $search = $request['search'];

        $results = Lobject::with('los')
            ->join('lobject_state', 'lobjects.id', '=', 'lobject_state.lobject_id')
            ->join('states', 'lobject_state.state_id', '=', 'states.id')
            ->whereColumn('lobjects.los_id', 'lobject_state.id')
            ->where(DB::raw('lower(title)'), 'like', DB::raw("lower('%{$search}%')"))
            ->where('states.id', '=', '2')
            ->orderBy('lobject_state.order', 'desc')
            ->paginate(20);
//        dd($results);

        return view('searches.basic', compact('results'));
    }
}
